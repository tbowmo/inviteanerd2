import { AuthState } from './auth.models';
import { authLoginFailed, authLoginSuccess, authLogout } from './auth.actions';
import { createReducer, on, Action } from '@ngrx/store';

export const initialState: AuthState = {
  isAuthenticated: false,
  token: ''
};

const reducer = createReducer(
  initialState,
  on(authLogout, (state) => ({ ...state, isAuthenticated: false, token: '' })),
  on(authLoginFailed, (state) => ({
    ...state,
    isAuthenticated: false,
    token: ''
  })),
  on(authLoginSuccess, (state, { token }) => ({
    ...state,
    isAuthenticated: true,
    token: token
  }))
);

export function authReducer(
  state: AuthState | undefined,
  action: Action
): AuthState {
  return reducer(state, action);
}
