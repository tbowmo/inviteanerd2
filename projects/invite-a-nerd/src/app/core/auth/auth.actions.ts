import { createAction, props } from '@ngrx/store';

export const authLogin = createAction('[Auth] Login');
export const authLogout = createAction('[Auth] Logout');

export const authLoginSuccess = createAction(
  '[Auth] login succes',
  props<{ token: string }>()
);

export const authLoginFailed = createAction('[Auth] failed');
