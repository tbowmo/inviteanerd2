import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  LoginModalComponent,
  Credentials
} from './login-modal/login-modal.component';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private dialog: MatDialog) {}

  public login(user: string, password: string): Observable<string> {
    return this.http.post<string>(environment.backend + '/authenticate', {
      username: user,
      password: password
    });
  }

  public openLoginWindow(): Observable<Credentials | undefined> {
    return this.dialog
      .open(LoginModalComponent, {
        disableClose: true
      })
      .afterClosed();
  }
}
