import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
export interface Credentials {
  user: string;
  password: string;
}

@Component({
  selector: 'ian-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.css']
})
export class LoginModalComponent implements OnInit {
  hide = true;
  loginForm: FormGroup;
  @ViewChild('password') passInput: ElementRef;

  constructor(
    private dialogRef: MatDialogRef<LoginModalComponent>,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    if (this.loginForm.invalid) {
      return;
    }
    const cred: Credentials = {
      user: this.loginForm.value['user'],
      password: this.loginForm.value['password']
    };
    this.dialogRef.close(cred);
  }

  completeOnEnterKey(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      if (this.loginForm.value['password'] === '') {
        this.passInput.nativeElement.focus();
      } else {
        this.login();
      }
    }
  }

  close() {
    this.dialogRef.close();
  }
}
