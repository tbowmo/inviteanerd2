import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ofType, createEffect, Actions } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { LocalStorageService } from '../local-storage/local-storage.service';

import {
  authLogin,
  authLoginFailed,
  authLoginSuccess,
  authLogout
} from './auth.actions';
import { AuthService } from './auth.service';

export const AUTH_KEY = 'AUTH';

export interface AuthSettings {
  isAuthenticated: boolean;
  token: string;
}

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private localStorageService: LocalStorageService,
    private router: Router,
    private loginService: AuthService
  ) {}

  login = createEffect(() =>
    this.actions$.pipe(
      ofType(authLogin),
      switchMap(() => this.loginService.openLoginWindow()),
      switchMap((result) => {
        if (result !== undefined) {
          return this.loginService.login(result.user, result.password);
        }
        return of('');
      }),
      map((credentials: any) => {
        if (credentials.token !== undefined && credentials.token !== null) {
          return authLoginSuccess({ token: credentials.token });
        }
        return authLoginFailed();
      })
    )
  );

  loginSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authLoginSuccess),
        tap((result) => {
          const data: AuthSettings = {
            isAuthenticated: true,
            token: result.token
          };
          this.localStorageService.setItem(AUTH_KEY, data);
        })
      ),
    { dispatch: false }
  );

  logout = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authLogout),
        tap(() => {
          this.router.navigate(['']);
          const data: AuthSettings = {
            isAuthenticated: false,
            token: ''
          };
          this.localStorageService.setItem(AUTH_KEY, data);
        })
      ),
    { dispatch: false }
  );
}
