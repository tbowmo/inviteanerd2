import { EntityState } from '@ngrx/entity';

export interface Brew {
  id: number;
  name: string;
}

export interface BrewState extends EntityState<Brew> {}
