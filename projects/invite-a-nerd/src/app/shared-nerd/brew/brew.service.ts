import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Brew } from './brew.model';

@Injectable()
export class BrewService {
  constructor(private http: HttpClient) {}

  getBrews(): Observable<Brew[]> {
    return this.http.get<Brew[]>(environment.backend + 'brew/list');
  }

  addBrew(brew: Brew): Observable<Brew> {
    return this.http.post<Brew>(environment.backend + 'brew', brew);
  }
}
