import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { NerdsiteModel } from '../models/address';
import * as actions from './nerd-site.actions';
import { Action, createReducer, on } from '@ngrx/store';

export interface NerdSiteModelState extends EntityState<NerdsiteModel> {
  lastAddedNerdsite?: NerdsiteModel;
  loading: boolean;
}

export const nerdsiteAdapter: EntityAdapter<NerdsiteModel> = createEntityAdapter<
  NerdsiteModel
>();

export const initialState: NerdSiteModelState = nerdsiteAdapter.getInitialState(
  {
    lastAddedNerdsite: undefined,
    loading: false
  }
);

const reducer = createReducer(
  initialState,
  on(actions.siteLoad, (state) => ({ ...state, loading: true })),
  on(actions.siteLoadSuccess, (state, { nerdSites }) => {
    nerdsiteAdapter.removeAll(state);
    return nerdsiteAdapter.setAll(nerdSites, {
      ...state,
      loading: false
    });
  }),
  on(actions.siteUpdateSuccess, (state, { nerdSite }) =>
    nerdsiteAdapter.upsertOne(nerdSite, {
      ...state,
      lastAddedNerdsite: nerdSite
    })
  )
);

export function nerdSiteReducer(
  state: NerdSiteModelState,
  action: Action
): NerdSiteModelState {
  return reducer(state, action);
}
