import { nerdSiteReducer, initialState } from './nerd-site.reducer';

describe('NerdSite Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = nerdSiteReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
