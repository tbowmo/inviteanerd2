import { TestBed } from '@angular/core/testing';

import { NerdSiteService } from './nerd-site.service';

describe('NerdSiteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NerdSiteService = TestBed.inject(NerdSiteService);
    expect(service).toBeTruthy();
  });
});
