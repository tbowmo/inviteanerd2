import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { NerdSiteService } from './nerd-site.service';
import { of } from 'rxjs';
import * as actions from './nerd-site.actions';
import { map, switchMap, catchError } from 'rxjs/operators';
import { NerdsiteModel } from '../models/address';

@Injectable()
export class NerdSiteEffects {
  constructor(
    private actions$: Actions<Action>,
    private nerdSiteService: NerdSiteService
  ) {}

  loadAllNerdSites = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.siteLoad),
      switchMap((action) =>
        this.nerdSiteService
          .getAll(action.nerdId, action.deleted)
          .pipe(catchError((_err) => of(null)))
      ),
      map((nerdSites: NerdsiteModel[]) =>
        actions.siteLoadSuccess({ nerdSites })
      )
    )
  );

  addUpdateNerdsite = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.siteUpdate),
      map((action) => action.nerdSite),
      switchMap((nerdsite: NerdsiteModel) =>
        this.nerdSiteService
          .addOrUpdate(nerdsite)
          .pipe(catchError((_err) => of(null)))
      ),
      map((nerdSite: NerdsiteModel) => actions.siteUpdateSuccess({ nerdSite }))
    )
  );
}
