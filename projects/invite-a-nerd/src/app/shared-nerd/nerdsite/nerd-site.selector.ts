import { createSelector } from '@ngrx/store';

import {
  selectSharedNerd,
  SharedNerdState
} from '../../shared-nerd/shared-nerd.state';

import { nerdsiteAdapter, NerdSiteModelState } from './nerd-site.reducer';

const { selectAll } = nerdsiteAdapter.getSelectors();

export const selectNerdsites = createSelector(
  selectSharedNerd,
  (state: SharedNerdState) => state.nerdsites
);

export const lastAddedNerdSite = createSelector(
  selectNerdsites,
  (state: NerdSiteModelState) => state.lastAddedNerdsite
);

export const selectAllNerdsites = createSelector(selectNerdsites, selectAll);

export const selectNerdsitesLoading = createSelector(
  selectNerdsites,
  (state) => state.loading
);
