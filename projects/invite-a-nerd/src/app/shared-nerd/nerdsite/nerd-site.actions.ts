import { createAction, props } from '@ngrx/store';
import { NerdsiteModel } from '../';

export const siteLoad = createAction(
  '[Site] load',
  props<{ nerdId: number; deleted?: boolean }>()
);
export const siteLoadSuccess = createAction(
  '[Site] load success',
  props<{ nerdSites: NerdsiteModel[] }>()
);
export const siteUpdate = createAction(
  '[Site] update',
  props<{ nerdSite: NerdsiteModel }>()
);
export const siteUpdateSuccess = createAction(
  '[Site] update success',
  props<{ nerdSite: NerdsiteModel }>()
);
export const siteDelete = createAction(
  '[Site] delete',
  props<{ nerdSite: NerdsiteModel }>()
);
