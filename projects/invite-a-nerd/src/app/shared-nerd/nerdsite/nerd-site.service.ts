import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NerdsiteModel } from '../models/address';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NerdSiteService {
  constructor(private http: HttpClient) {}

  getAll(nerd: number, deleted: boolean = false): Observable<NerdsiteModel[]> {
    return this.http.get<NerdsiteModel[]>(
      environment.backend + '/nerdsite/' + nerd + (deleted ? '/1' : '')
    );
  }

  addOrUpdate(nerdSite: NerdsiteModel): Observable<NerdsiteModel> {
    return this.http.post<NerdsiteModel>(
      environment.backend + '/nerdsite',
      nerdSite
    );
  }
}
