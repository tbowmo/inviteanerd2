export interface Address {
  id: number;
  name: string;
  address: string;
  x: number;
  y: number;
  deleted?: boolean;
}

export interface NerdsiteModel extends Address {
  nerdid: number;
}

export interface RestaurantModel extends Address {
  phone: string;
  link: string;
  distance?: number;
}
