import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import { AppState } from '../core/core.module';
import { nerdReducer } from './nerd/nerd.reducer';
import { NerdModelState } from './nerd/nerd.reducer';
import {
  NerdSiteModelState,
  nerdSiteReducer
} from './nerdsite/nerd-site.reducer';
import {
  RestaurantModelState,
  restaurantReducer
} from './restaurant/restaurant.reducer';

export const FEATURE_NAME = 'nerd';
export const selectSharedNerd = createFeatureSelector<State, SharedNerdState>(
  FEATURE_NAME
);

export const reducers: ActionReducerMap<SharedNerdState> = {
  nerds: nerdReducer,
  nerdsites: nerdSiteReducer,
  restaurants: restaurantReducer
};

export interface SharedNerdState {
  nerds: NerdModelState;
  nerdsites: NerdSiteModelState;
  restaurants: RestaurantModelState;
}

export interface State extends AppState {
  nerd: SharedNerdState;
}
