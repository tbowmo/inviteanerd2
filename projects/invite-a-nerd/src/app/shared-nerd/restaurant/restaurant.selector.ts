import { createSelector } from '@ngrx/store';

import {
  selectSharedNerd,
  SharedNerdState
} from '../../shared-nerd/shared-nerd.state';

import { restaurantAdapter, RestaurantModelState } from './restaurant.reducer';

export const { selectAll } = restaurantAdapter.getSelectors();

const selectRestaurants = createSelector(
  selectSharedNerd,
  (state: SharedNerdState) => state.restaurants
);

export const selectAllRestaurants = createSelector(
  selectRestaurants,
  selectAll
);

export const lastAddedRestaurant = createSelector(
  selectRestaurants,
  (state: RestaurantModelState) => state.lastAddedRestaurant
);

export const selectRestaurantsLoading = createSelector(
  selectRestaurants,
  (state) => state.loading
);
