import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';
import { RestaurantModel } from '../models/address';
import { RestaurantService } from './restaurant.service';
import * as actions from './restaurant.actions';

@Injectable()
export class RestaurantEffects {
  constructor(
    private actions$: Actions<Action>,
    private restaurantService: RestaurantService
  ) {}

  loadRestaurants = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.restaurantLoad),
      switchMap((action) =>
        this.restaurantService
          .getAll(action.nerdsiteId, action.distance)
          .pipe(catchError((_err) => of(null)))
      ),
      map((restaurants: RestaurantModel[]) =>
        actions.restaurantLoadSuccess({ restaurants })
      )
    )
  );

  addUpdateRestaurant = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.restaurantUpdate),
      map((action) => action.restaurant),
      switchMap((restaurant: RestaurantModel) =>
        this.restaurantService
          .addUpdate(restaurant)
          .pipe(catchError((_err) => of(null)))
      ),
      map((restaurant: RestaurantModel) =>
        actions.restaurantUpdateSuccess({ restaurant })
      )
    )
  );
}
