import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';
import { RestaurantModel } from '../';
import * as actions from './restaurant.actions';

export interface RestaurantModelState extends EntityState<RestaurantModel> {
  lastAddedRestaurant?: RestaurantModel;
  loading: boolean;
  // additional entities state properties
}

export const restaurantAdapter: EntityAdapter<RestaurantModel> = createEntityAdapter<
  RestaurantModel
>();

export const initialState: RestaurantModelState = restaurantAdapter.getInitialState(
  {
    lastAddedRestaurant: undefined,
    loading: false
    // additional entity state properties
  }
);

const reducer = createReducer(
  initialState,
  on(actions.restaurantLoad, (state) => ({ ...state, loading: true })),
  on(actions.restaurantLoadSuccess, (state, { restaurants }) => {
    restaurantAdapter.removeAll(state);
    return restaurantAdapter.setAll(restaurants, {
      ...state,
      loading: false
    });
  }),
  on(actions.restaurantUpdateSuccess, (state, { restaurant }) =>
    restaurantAdapter.upsertOne(restaurant, {
      ...state,
      lastAddedRestaurant: restaurant
    })
  )
);
export function restaurantReducer(
  state: RestaurantModelState,
  action: Action
): RestaurantModelState {
  return reducer(state, action);
}
