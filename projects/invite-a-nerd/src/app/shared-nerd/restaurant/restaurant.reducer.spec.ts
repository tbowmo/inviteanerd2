import { restaurantReducer, initialState } from './restaurant.reducer';

describe('Restaurant Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = restaurantReducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
