import { createAction, props } from '@ngrx/store';
import { RestaurantModel } from '../';

export const restaurantLoad = createAction(
  '[Restaurant] Load',
  props<{ nerdsiteId: number; distance: number }>()
);
export const restaurantLoadSuccess = createAction(
  '[Restaurant] Load success',
  props<{ restaurants: RestaurantModel[] }>()
);
export const restaurantUpdate = createAction(
  '[Restautant] Update',
  props<{ restaurant: RestaurantModel }>()
);
export const restaurantUpdateSuccess = createAction(
  '[Restautant] Update success',
  props<{ restaurant: RestaurantModel }>()
);
