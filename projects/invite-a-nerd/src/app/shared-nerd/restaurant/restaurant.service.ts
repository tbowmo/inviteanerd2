import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestaurantModel } from '../models/address';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  constructor(private http: HttpClient) {}

  getAll(nerdSite: number, distance: number): Observable<RestaurantModel[]> {
    return this.http.get<RestaurantModel[]>(
      environment.backend + `restaurant/${nerdSite}/${distance}`
    );
  }
  addUpdate(restaurant: RestaurantModel): Observable<RestaurantModel> {
    return this.http.post<RestaurantModel>(
      environment.backend + '/restaurant',
      restaurant
    );
  }

  delete(restaurant: RestaurantModel): Observable<boolean> {
    return of(false);
  }
}
