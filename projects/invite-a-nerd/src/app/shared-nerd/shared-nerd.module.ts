import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { FEATURE_NAME, reducers } from './shared-nerd.state';
import { NerdEffects } from './nerd/nerd.effects';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { NerdSiteEffects } from './nerdsite/nerd-site.effects';
import { RestaurantEffects } from './restaurant/restaurant.effects';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(FEATURE_NAME, reducers),
    EffectsModule.forFeature([NerdEffects, NerdSiteEffects, RestaurantEffects])
  ]
})
export class SharedNerdModule {}
