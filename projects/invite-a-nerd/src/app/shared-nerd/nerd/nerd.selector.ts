import { createSelector } from '@ngrx/store';

import { selectRouterState } from '../../core/core.module';

import { selectSharedNerd, SharedNerdState } from '../shared-nerd.state';

import { nerdsAdapter } from './nerd.reducer';

const { selectEntities, selectAll } = nerdsAdapter.getSelectors();

export const selectNerds = createSelector(
  selectSharedNerd,
  (state: SharedNerdState) => state.nerds
);

export const selectAllNerds = createSelector(selectNerds, selectAll);
export const selectNerdEntities = createSelector(selectNerds, selectEntities);

export const selectCurrentNerd = createSelector(
  selectNerds,
  (state) => state.currentNerd
);

export const selectSelectedNerd = createSelector(
  selectNerdEntities,
  selectRouterState,
  (entities, params) => params && entities[params.state.params.id]
);

export const selectNerdsLoading = createSelector(
  selectNerds,
  (state) => state.loading
);
