import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as action from './nerd.actions';
import { of } from 'rxjs';
import { switchMap, map, mergeMap, tap, catchError } from 'rxjs/operators';
import { NerdService } from './nerd.service';
import { NerdModel } from './nerd.model';
import { NotificationService } from '../../core/core.module';

@Injectable()
export class NerdEffects {
  constructor(
    private actions$: Actions,
    private nerdService: NerdService,
    private notifications: NotificationService
  ) {}

  loadNerds = createEffect(() =>
    this.actions$.pipe(
      ofType(action.nerdLoadAll),
      // withLatestFrom(this.store$),
      // filter(([_action, store]) => {
      //   return store.nerd.nerds.ids.length < 10;
      // }),
      mergeMap(() =>
        this.nerdService.getNerds().pipe(catchError((_err) => of(null)))
      ),
      map((nerds: NerdModel[]) => action.nerdLoadAllSuccess({ nerds }))
    )
  );

  loadSingleNerd = createEffect(() =>
    this.actions$.pipe(
      ofType(action.nerdLoadSingle),
      map((nerdLoad) => nerdLoad.nerdId),
      switchMap((nerdId) =>
        this.nerdService.getNerd(nerdId).pipe(catchError((_err) => of(null)))
      ),
      map((nerd: NerdModel) => action.nerdLoadSingleSuccess({ nerd }))
    )
  );

  addOrUpdateNerd = createEffect(() =>
    this.actions$.pipe(
      ofType(action.nerdUpdate),
      map((nerdUpdate) => nerdUpdate.nerd),
      switchMap((nerd) =>
        this.nerdService.saveNerd(nerd).pipe(catchError((_err) => of(null)))
      ),
      map((nerd: NerdModel) => action.nerdUpdateSuccess({ nerd }))
    )
  );

  addOrUpdateSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(action.nerdUpdateSuccess),
        map((nerdUpdateSuccess) => nerdUpdateSuccess.nerd),
        tap((nerd) => {
          this.notifications.success('Opdateret ' + nerd.name);
        })
      ),
    { dispatch: false }
  );
}
