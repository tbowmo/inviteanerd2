import { nerdReducer, INIT_STATE } from './nerd.reducer';

describe('Nerd Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = nerdReducer(INIT_STATE, action);

      expect(result).toBe(INIT_STATE);
    });
  });
});
