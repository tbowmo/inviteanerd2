import { Action, createAction, props } from '@ngrx/store';
import { NerdModel } from './nerd.model';

export const nerdLoadAll = createAction('[Nerd] Load all');
export const nerdLoadAllSuccess = createAction(
  '[Nerd] Load all success',
  props<{ nerds: NerdModel[] }>()
);

export const nerdLoadSingle = createAction(
  '[Nerd] load single',
  props<{ nerdId: number }>()
);
export const nerdLoadSingleSuccess = createAction(
  '[Nerd] load single success',
  props<{ nerd: NerdModel }>()
);

export const nerdUpdate = createAction(
  '[Nerd] update',
  props<{ nerd: NerdModel }>()
);
export const nerdUpdateSuccess = createAction(
  '[Nerd] update success',
  props<{ nerd: NerdModel }>()
);

export const nerdClearCurrent = createAction('[Nerd] clear current');
