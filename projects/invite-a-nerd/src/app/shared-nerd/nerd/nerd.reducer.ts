import { createEntityAdapter, EntityState } from '@ngrx/entity';

import { NerdModel } from './nerd.model';
import * as actions from './nerd.actions';
import { createReducer, on, Action } from '@ngrx/store';
import { nerdSiteReducer } from '../nerdsite/nerd-site.reducer';

export const nerdsAdapter = createEntityAdapter<NerdModel>({
  selectId: (nerd: NerdModel) => nerd.id,
  sortComparer: sortByName
});

export function sortByName(a: NerdModel, b: NerdModel): number {
  return a.name.localeCompare(b.name);
}

export interface NerdModelState extends EntityState<NerdModel> {
  currentNerd?: NerdModel;
  loading: boolean;
}

export const INIT_STATE: NerdModelState = nerdsAdapter.getInitialState({
  currentNerd: undefined,
  loading: false
});

const reducer = createReducer(
  INIT_STATE,
  on(actions.nerdLoadAll, (state) => ({ ...state, loading: true })),
  on(actions.nerdLoadAllSuccess, (state, { nerds }) =>
    nerdsAdapter.setAll(nerds, { ...state, loading: false })
  ),
  on(actions.nerdLoadSingle, (state) => ({ ...state, loading: true })),
  on(actions.nerdLoadSingleSuccess, (state, { nerd }) =>
    nerdsAdapter.upsertOne(nerd, {
      ...state,
      currentNerd: nerd,
      loading: false
    })
  ),
  on(actions.nerdUpdateSuccess, (state, { nerd }) =>
    nerdsAdapter.upsertOne(nerd, {
      ...state,
      currentNerd: nerd,
      loading: false
    })
  )
);

export function nerdReducer(
  state: NerdModelState,
  action: Action
): NerdModelState {
  return reducer(state, action);
}
