export interface NerdModel {
  id: number;
  name: string;
  phone: string;
  email: string;
  preferedSodaId: number;
  soda?: string;
  mailnotification: boolean;
  suspended: boolean;
  homepageurl?: string;
  image?: string;
  frontpage: boolean;
  donoteatpizza: boolean;
  imageData?: string;
}
