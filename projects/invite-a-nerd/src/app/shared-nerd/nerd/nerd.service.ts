import { Injectable } from '@angular/core';
import { NerdModel } from './nerd.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NerdService {
  constructor(private http: HttpClient) {}

  getNerds(): Observable<NerdModel[]> {
    return this.http.get<NerdModel[]>(environment.backend + 'nerd/list');
  }

  getNerd(id: number): Observable<NerdModel> {
    return this.http.get<NerdModel>(environment.backend + 'nerd/' + id);
  }

  saveNerd(nerd: NerdModel): Observable<NerdModel> {
    return this.http.post<NerdModel>(environment.backend + 'nerd', nerd);
  }

  deleteNerd(nerd: NerdModel) {
    return this.http.delete<boolean>(environment.backend + 'nerd/' + nerd.id);
  }
}
