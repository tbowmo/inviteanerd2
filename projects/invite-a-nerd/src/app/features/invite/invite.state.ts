import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import { inviteReducer } from './store/invitation/invitation.reducer';
import { InviteState as InvitationState } from './store/invitation/invitation.reducer';
import { answerReducer } from './store/answer/answer.reducer';
import { AnswerState } from './store/answer/answer.reducer';
import { State as NerdState } from '../../shared-nerd/shared-nerd.state';
import { toppingReducer } from './store/topping/topping.reducer';
import { State as ToppingState } from './store/topping/topping.reducer';

export const FEATURE_NAME = 'invite';
export const selectInvite = createFeatureSelector<State, InviteState>(
  FEATURE_NAME
);

export const reducers: ActionReducerMap<InviteState> = {
  invitation: inviteReducer,
  answer: answerReducer,
  topping: toppingReducer
};

export interface InviteState {
  invitation: InvitationState;
  answer: AnswerState;
  topping: ToppingState;
}

export interface State extends NerdState {
  invite: InviteState;
}
