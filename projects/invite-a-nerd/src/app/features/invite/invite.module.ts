import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { AnswerEffects } from './store/answer/answer.effects';
import { AnswersComponent } from './detailed-view/answers/answers.component';
import { AnswerService } from './store/answer/answer.service';
import { BottomSheetComponent } from './detailed-view/bottom-sheet/bottom-sheet.component';
import { CommonModule } from '@angular/common';
import { DetailedViewComponent } from './detailed-view/detailed-view.component';
import { EffectsModule } from '@ngrx/effects';
import { InvitationEffects } from './store/invitation/invitation.effects';
import { InvitationService } from './store/invitation/invitation.service';
import { InviteComponent } from './invite/invite.component';
import { InviteDetailComponent } from './invite-detail/invite-detail.component';
import { InviteEditComponent } from './invite-edit/invite-edit.component';
import { InviteEffects } from './invite.effects';
import { InviteListComponent } from './invite-list/invite-list.component';
import { InviteRoutingModule } from './invite-routing.module';
import { PizzaOrderComponent } from './detailed-view/answers/pizza-order/pizza-order.component';
import { reducers, FEATURE_NAME } from './invite.state';
import { SharedModule } from '../../shared/shared.module';
import { SharedNerdModule } from '../../shared-nerd';
import { StatsComponent } from './detailed-view/stats/stats.component';
import { StoreModule } from '@ngrx/store';
import { ToppingEffects } from './store/topping/topping.effects';
import { ToppingService } from './store/topping/topping.service';
import { UpcommingComponent } from './upcomming/upcomming.component';
import { ActionBarComponent } from './detailed-view/action-bar/action-bar.component';
import { AddGuestComponent } from './detailed-view/answers/add-guest/add-guest.component';
import { environment } from '../../../environments/environment';
import {
  ModuleTranslateLoader,
  IModuleTranslationOptions
} from '@larscom/ngx-translate-module-loader';

export function moduleHttpLoaderFactory(http: HttpClient) {
  const baseTranslateUrl = `${environment.i18nPrefix}/assets/i18n`;

  const options: IModuleTranslationOptions = {
    lowercaseNamespace: true,
    modules: [
      // final url: ./assets/i18n/en.json
      { baseTranslateUrl },
      // final url: ./assets/i18n/feature1/en.json
      { moduleName: 'invite', baseTranslateUrl }
    ]
  };

  return new ModuleTranslateLoader(http, options);
}

@NgModule({
  imports: [
    SharedModule,
    InviteRoutingModule,
    SharedNerdModule,
    StoreModule.forFeature(FEATURE_NAME, reducers),
    EffectsModule.forFeature([
      InvitationEffects,
      AnswerEffects,
      InviteEffects,
      ToppingEffects
    ]),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: moduleHttpLoaderFactory,
        deps: [HttpClient]
      },
      isolate: true
    }),
    CommonModule
  ],
  entryComponents: [
    PizzaOrderComponent,
    BottomSheetComponent,
    AddGuestComponent
  ],
  providers: [AnswerService, InvitationService, ToppingService],
  declarations: [
    AnswersComponent,
    DetailedViewComponent,
    InviteComponent,
    InviteDetailComponent,
    InviteEditComponent,
    InviteListComponent,
    PizzaOrderComponent,
    UpcommingComponent,
    BottomSheetComponent,
    StatsComponent,
    ActionBarComponent,
    AddGuestComponent
  ]
})
export class InviteModule {
  constructor() {}
}
