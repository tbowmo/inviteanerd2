import { Component } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
  template: `
    <div class="row">
      <button mat-mini-fab>
        <fa-icon icon="caret-down" (click)="close()"></fa-icon>
      </button>
      <ian-stats></ian-stats>
    </div>
  `,
  styles: ['ian-stats {width:100%;} ian-action-bar {width:100%;}']
})
export class BottomSheetComponent {
  constructor(
    private bottomSheetRef: MatBottomSheetRef<BottomSheetComponent>
  ) {}

  close() {
    this.bottomSheetRef.dismiss();
  }
}
