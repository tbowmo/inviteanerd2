import { Component, OnInit, Inject } from '@angular/core';
import { AnswerModel, GuestModel } from '../../../store/answer/answer.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'ian-add-guest',
  templateUrl: './add-guest.component.html',
  styleUrls: ['./add-guest.component.css']
})
export class AddGuestComponent implements OnInit {
  guestForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddGuestComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AnswerModel
  ) {}

  ngOnInit() {
    this.guestForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
  }

  save(): void {
    if (this.guestForm.valid) {
      const guest: GuestModel = {
        invitationid: this.data.invitationid,
        nerdid: this.data.nerdid,
        name: this.guestForm.get('name').value,
        id: 0
      };
      this.dialogRef.close(guest);
    }
  }
}
