import { AddGuestComponent } from './add-guest.component';
import { AnswerModel, GuestModel } from '../../../store/answer/answer.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

@Injectable()
export class AddGuestService {
  constructor(private dialog: MatDialog) {}

  open(answer: AnswerModel): Observable<GuestModel | undefined> {
    return this.dialog
      .open(AddGuestComponent, {
        width: '400px',
        data: answer
      })
      .afterClosed();
  }
}
