import { Injectable } from '@angular/core';
import { PizzaOrderComponent } from './pizza-order.component';
import { GuestModel, AnswerModel } from '../../../store/answer/answer.model';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

@Injectable()
export class PizzaOrderService {
  constructor(private dialog: MatDialog) {}

  open(order: AnswerModel): Observable<AnswerModel | undefined>;
  open(order: GuestModel): Observable<GuestModel | undefined>;
  open<T>(order: T): Observable<T | undefined> {
    return this.dialog
      .open(PizzaOrderComponent, {
        width: '400px',
        data: order
      })
      .afterClosed();
  }
}
