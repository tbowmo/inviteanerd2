import { TestBed } from '@angular/core/testing';

import { AddGuestService } from './add-guest.service';

describe('AddGuestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddGuestService = TestBed.inject(AddGuestService);
    expect(service).toBeTruthy();
  });
});
