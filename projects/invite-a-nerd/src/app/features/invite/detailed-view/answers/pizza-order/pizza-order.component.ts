import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { AnswerModel, GuestModel } from '../../../store/answer/answer.model';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { ToppingModel } from '../../../store/topping/topping.model';
import { Store, select } from '@ngrx/store';
import { State } from '../../../invite.state';
import { selectAllToppings } from '../../../store/topping/topping.selector';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InvitationModel } from '../../../store/invitation/invitation.model';
import { selectCurrentInvitation } from '../../../store/invitation/invitation.selector';
import { selectSingleAnswer } from '../../../store/answer/answer.selector';
import {
  take,
  switchMap,
  startWith,
  debounceTime,
  distinctUntilChanged,
  map,
  filter
} from 'rxjs/operators';
import { AnswerService } from '../../../store/answer/answer.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'ian-pizza-order',
  templateUrl: './pizza-order.component.html',
  styleUrls: ['./pizza-order.component.scss']
})
export class PizzaOrderComponent implements OnInit, OnDestroy {
  topping$: Observable<ToppingModel[]>;
  pizzaForm: FormGroup;
  invite$: Observable<InvitationModel>;
  answer$: Observable<AnswerModel | GuestModel>;
  filteredPizza$: Observable<string[]>;

  private pizzaHistory$: Observable<string[]>;
  private sub: Subscription[] = [];

  constructor(
    private store: Store<State>,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<PizzaOrderComponent>,
    private answerService: AnswerService,
    @Inject(MAT_DIALOG_DATA) public data: AnswerModel | GuestModel
  ) {
    this.topping$ = this.store.pipe(select(selectAllToppings));
    this.invite$ = this.store.pipe(select(selectCurrentInvitation));
    this.answer$ = this.store.pipe(
      select(selectSingleAnswer, { id: this.data.nerdid }),
      map((answer: AnswerModel) => {
        if (this.isNerd(this.data)) {
          return answer;
        }
        return answer.guest.find(
          (guest) => guest.id === (this.data as GuestModel).id
        );
      })
    );
  }

  ngOnInit() {
    this.pizzaHistory$ = combineLatest([this.invite$, this.answer$]).pipe(
      filter(([_invite, answer]) => this.isNerd(answer)),
      switchMap(([invite, answer]) =>
        this.answerService.pizzaHistory(answer as AnswerModel, invite)
      )
    );

    this.sub.push(
      this.invite$.subscribe((invite) => {
        const isRequired = invite.restaurant ? ['', Validators.required] : [];
        this.pizzaForm = this.formBuilder.group({
          noeating: [],
          eatingno: isRequired,
          price: isRequired,
          topping: []
        });
      })
    );

    this.sub.push(
      this.pizzaForm
        .get('noeating')
        .valueChanges.subscribe((status: boolean) => {
          if (status) {
            this.pizzaForm.get('eatingno').disable();
            this.pizzaForm.get('topping').disable();
            this.pizzaForm.get('price').disable();
          } else {
            this.pizzaForm.get('eatingno').enable();
            this.pizzaForm.get('topping').enable();
            this.pizzaForm.get('price').enable();
          }
        })
    );

    this.filteredPizza$ = this.pizzaForm.get('eatingno').valueChanges.pipe(
      startWith(''),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((filterValue: string) => {
        return this.pizzaHistory$.pipe(
          map((pizzas) =>
            pizzas.filter((pizza) =>
              pizza
                .toLocaleLowerCase()
                .includes(filterValue.toLocaleLowerCase())
            )
          )
        );
      })
    );

    this.answer$.pipe(take(1)).subscribe((answer) => {
      this.pizzaForm.patchValue(answer);
    });
  }

  ngOnDestroy() {
    this.sub.forEach((s) => s.unsubscribe());
  }

  save(): void {
    this.answer$.pipe(take(1)).subscribe((answer) => {
      const a = {
        ...answer,
        ...this.pizzaForm.value
      };
      this.dialogRef.close(a);
    });
  }

  isNerd(i: AnswerModel | GuestModel): i is AnswerModel {
    return 'attending' in i;
  }

  isGuest(i: AnswerModel | GuestModel): i is GuestModel {
    return 'id' in i;
  }
}
