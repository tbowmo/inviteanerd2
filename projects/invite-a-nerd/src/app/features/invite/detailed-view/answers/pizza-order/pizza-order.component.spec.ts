import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteModule } from '../../../invite.module';
import { Component, DebugElement } from '@angular/core';

@Component({
  selector: 'ian-host-for-test',
  template: ` <ian-pizza-order></ian-pizza-order> `
})
class HostComponent {}
xdescribe('PizzaOrderComponent', () => {
  let component: HostComponent;
  let fixture: ComponentFixture<HostComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HostComponent],
      imports: [InviteModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement.childNodes[0] as DebugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
