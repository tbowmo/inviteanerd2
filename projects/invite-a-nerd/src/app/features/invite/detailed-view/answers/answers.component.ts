import {
  AnswerModel,
  AnswerAttending,
  GuestModel
} from '../../store/answer/answer.model';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { inviteCreateOrUpdate } from '../../store/invitation/invitation.actions';
import { InvitationModel } from '../../store/invitation/invitation.model';
import { toppingLoad } from '../../store/topping/topping.actions';
import { map, take } from 'rxjs/operators';
import { Observable, Subscription, combineLatest } from 'rxjs';
import {
  selectAllAnswers,
  selectAccountingState,
  selectAnswersLoadingIndicator,
  selectUpdateInProgress
} from '../../store/answer/answer.selector';
import { selectAllToppings } from '../../store/topping/topping.selector';
import { selectCurrentInvitation } from '../../store/invitation/invitation.selector';
import { State } from '../../invite.state';
import { Store, select } from '@ngrx/store';
import {
  trigger,
  state,
  transition,
  style,
  animate
} from '@angular/animations';
import {
  answerUpdate,
  answerUpdateGuest,
  answerDeleteGuest
} from '../../store/answer/answer.actions';
import { DomSanitizer } from '@angular/platform-browser';
import { PizzaOrderService } from './pizza-order/pizza-order.service';
import { AddGuestService } from './add-guest/add-guest.service';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'ian-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.scss'],
  animations: [
    trigger('detailExpand', [
      state(
        'collapsed',
        style({ height: '0px', minHeight: '0', display: 'none' })
      ),
      state('expanded', style({ height: '100px' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      )
    ])
  ],
  providers: [PizzaOrderService, AddGuestService]
})
export class AnswersComponent implements OnInit, OnDestroy {
  invite: InvitationModel;
  loading$: Observable<boolean>;

  afstemning$: Observable<boolean>;
  updating$: Observable<boolean>;

  answers$: Observable<(AnswerModel | GuestModel)[]>;
  expandedElement: AnswerModel;

  displayedColumns: string[] = ['name', 'phone', 'pizza', 'action'];
  sub: Subscription[] = [];

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map((result) => result.matches));

  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<State>,
    private sanitizer: DomSanitizer,
    private orderService: PizzaOrderService,
    private guestService: AddGuestService,
    private confirmService: ConfirmDialogService
  ) {
    this.afstemning$ = this.store.pipe(select(selectAccountingState));
    this.loading$ = this.store.pipe(select(selectAnswersLoadingIndicator));
    this.updating$ = this.store.pipe(select(selectUpdateInProgress));

    this.answers$ = combineLatest([
      this.store.pipe(select(selectAllAnswers)),
      this.store.pipe(select(selectAllToppings)).pipe(
        map((topping) => {
          return topping.reduce<string[]>((arr, top) => {
            arr[top.id] = top.name;
            return arr;
          }, []);
        })
      )
    ]).pipe(
      map(([answers, toppings]) => {
        return answers.reduce((answerArray: AnswerModel[], singleAnswer) => {
          const a: AnswerModel = {
            ...singleAnswer,
            eatingno: this.addTopping(singleAnswer, toppings),
            guest: singleAnswer.guest.map((guest) => {
              return {
                ...guest,
                eatingno: this.addTopping(guest, toppings)
              };
            })
          };
          answerArray.push(a);
          return answerArray;
        }, []);
      }),
      map((answer) => {
        const rows = [];
        answer.forEach((element) => {
          rows.push(element, { detailRow: true, element });
          if (element.attending === AnswerAttending.ACCEPTED) {
            element.guest.forEach((guest) => {
              rows.push(guest);
            });
          }
        });
        return rows;
      })
    );
  }

  /**
   *
   * @param answer
   * @param toppings
   */
  addTopping(answer: AnswerModel | GuestModel, toppings: string[]): string {
    let toppingstring = '';
    if (answer.topping !== undefined && answer.topping !== null) {
      answer.topping.forEach(($t) => {
        toppingstring =
          (toppingstring !== '' ? toppingstring + ',' : '') + toppings[$t];
      });
    }
    return (
      answer.eatingno + (toppingstring !== '' ? ' : ' + toppingstring : '')
    );
  }

  sortNerds(answer: AnswerModel[]): AnswerModel[] {
    return answer.sort((a, b) => {
      if (a.attending > b.attending) {
        return -1;
      } else if (a.attending < b.attending) {
        return +1;
      } else {
        if (a.nerd.name < b.nerd.name) {
          return -1;
        } else if (a.nerd.name > b.nerd.name) {
          return +1;
        }
      }
      return 0;
    });
  }

  accepted(answer: AnswerModel | GuestModel, _x?: boolean) {
    if (this.isNerd(answer)) {
      return answer.attending === AnswerAttending.ACCEPTED;
    }
    return true;
  }

  declined(answer: AnswerModel) {
    return answer.attending === AnswerAttending.DECLINED;
  }

  accept(answer: AnswerModel) {
    const a: AnswerModel = {
      ...answer,
      attending: AnswerAttending.ACCEPTED,
      noeating: answer.nerd.donoteatpizza
    };
    this.store.dispatch(answerUpdate({ answer: a }));
  }

  decline(answer: AnswerModel) {
    const a: AnswerModel = {
      ...answer,
      attending: AnswerAttending.DECLINED
    };
    this.store.dispatch(answerUpdate({ answer: a }));
  }

  pizzaOrderUpdate(answer: AnswerModel | GuestModel) {
    if (this.isNerd(answer)) {
      this.orderService
        .open(answer)
        .pipe(take(1))
        .subscribe((a) => {
          if (a) {
            this.store.dispatch(answerUpdate({ answer: a }));
          }
        });
    } else {
      this.orderService
        .open(answer)
        .pipe(take(1))
        .subscribe((a) => {
          if (a) {
            this.store.dispatch(answerUpdateGuest({ guest: a }));
          }
        });
    }
  }

  expand(answer: AnswerModel) {
    if (this.expandedElement === answer) {
      this.expandedElement = null;
    } else {
      this.expandedElement = answer;
    }
  }

  addGuest(answer: AnswerModel) {
    this.guestService
      .open(answer)
      .pipe(take(1))
      .subscribe((guest) => {
        if (guest) {
          this.store.dispatch(answerUpdateGuest({ guest: guest }));
        }
      });
  }

  deleteGuest(guest: GuestModel) {
    this.confirmService
      .openConfirmDialog('Vil du slette gæsten ' + guest.name)
      .pipe(take(1))
      .subscribe((b) => {
        if (b) {
          this.store.dispatch(answerDeleteGuest({ guest: guest }));
        }
      });
  }

  nerdHasPaidCheck(answer: AnswerModel): string {
    if (answer.haspaid || answer.noeating) {
      return 'primary';
    }
    return 'warn';
  }

  paid(answer: AnswerModel | GuestModel) {
    if (this.isNerd(answer)) {
      const a: AnswerModel = {
        ...answer,
        haspaid: !answer.haspaid
      };
      this.store.dispatch(answerUpdate({ answer: a }));
    } else {
      const a: GuestModel = {
        ...answer,
        haspaid: !answer.haspaid
      };
      this.store.dispatch(answerUpdateGuest({ guest: a }));
    }
  }

  mobilepay(answer: AnswerModel) {
    const i: InvitationModel = {
      ...this.invite,
      mobilepaynerdid: answer.nerd.id
    };
    this.store.dispatch(inviteCreateOrUpdate({ invitation: i }));
  }

  isExpansionDetailRow = (i: number, row: Object) =>
    row.hasOwnProperty('detailRow');

  isNerd(i: AnswerModel | GuestModel): i is AnswerModel {
    return 'attending' in i;
  }

  sms(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl('sms:' + url);
  }

  ngOnInit() {
    this.sub.push(
      combineLatest([this.isHandset$, this.afstemning$])
        .pipe(
          map(([handset, afsteming]) => {
            let p = 'pizza';
            let displayedColumns: string[] = [];
            if (afsteming) {
              p = 'afstemning';
            }
            if (handset) {
              displayedColumns = ['name', p, 'action'];
            } else {
              displayedColumns = ['name', 'phone', p, 'action'];
            }
            return { col: displayedColumns, afstemning: afsteming };
          })
        )
        .subscribe((columns) => {
          this.displayedColumns = columns.col;
        })
    );

    this.sub.push(
      this.store.pipe(select(selectCurrentInvitation)).subscribe((invite) => {
        this.invite = invite;
      })
    );
    this.store.dispatch(toppingLoad());
  }

  ngOnDestroy() {
    if (this.sub !== undefined) {
      this.sub.forEach((subscriber) => subscriber.unsubscribe());
    }
  }
}
