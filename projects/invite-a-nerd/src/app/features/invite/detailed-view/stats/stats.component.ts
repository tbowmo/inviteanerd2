import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Component, OnInit } from '@angular/core';
import { State } from '../../invite.state';
import { AnswerModel, GuestModel } from '../../store/answer/answer.model';
import { selectAllAnswers } from '../../store/answer/answer.selector';
import { InvitationModel } from '../../store/invitation/invitation.model';
import { selectCurrentInvitation } from '../../store/invitation/invitation.selector';
import { selectAllToppings } from '../../store/topping/topping.selector';
import { CountEntity } from '../../../../shared/count-table/count-table.component';
import { select, Store } from '@ngrx/store';

interface Counters {
  pizza: CountEntity[];
  drinks: CountEntity[];
  stats: CountEntity[];
}

@Component({
  selector: 'ian-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {
  counters$: Observable<Counters>;

  constructor(private store: Store<State>) {
    this.counters$ = combineLatest([
      this.store.pipe(select(selectCurrentInvitation)),
      this.store.pipe(select(selectAllAnswers)),
      this.store.pipe(select(selectAllToppings)).pipe(
        map((topping) => {
          return topping.reduce<string[]>((arr, top) => {
            arr[top.id] = top.name;
            return arr;
          }, []);
        })
      )
    ]).pipe(
      map(([invite, answers, toppings]) =>
        this.countNerds(answers, toppings, invite)
      )
    );
  }

  ngOnInit() {}

  countNerds(
    answers: AnswerModel[],
    toppings: string[],
    invite: InvitationModel
  ): Counters {
    let accepted = 0;
    let declined = 0;
    let noAnswer = 0;
    let debtors = 0;
    let guestCount = 0;
    const counter: Counters = {
      pizza: [],
      drinks: [],
      stats: []
    };
    answers.forEach((answerRO) => {
      const nerdAnswer: AnswerModel = {
        ...answerRO
      };
      if (!(nerdAnswer as object).hasOwnProperty('element')) {
        if (nerdAnswer.attending === 1) {
          declined++;
        }
        if (nerdAnswer.attending === 0) {
          noAnswer++;
        }
        if (nerdAnswer.attending === 2) {
          accepted++;
          if (!nerdAnswer.haspaid) {
            debtors += Number(nerdAnswer.price) + invite.contribution;
          }
          /*let foundSoda = false;
          counter.drinks.forEach(
            (value: CountEntity, index: number, b: CountEntity[]) => {
              if (value.label === a.nerd.soda) {
                value.count += 1;
                b[index] = value;
                foundSoda = true;
              }
            }
          );
          if (!foundSoda) {
            counter.drinks.push({ label: a.nerd.soda, count: 1 });
          }*/
          if (nerdAnswer.noeating) {
            this.pizzaCount(counter, 'invite.noeat');
          } else {
            if (!invite.restaurant) {
              this.pizzaCount(counter, 'invite.eathostsfood');
            } else if (nerdAnswer.eatingno !== '') {
              nerdAnswer.eatingno = this.addTopping(nerdAnswer, toppings);
              this.pizzaCount(counter, nerdAnswer.eatingno);
            } else {
              this.pizzaCount(counter, 'invite.noorder');
            }
          }
          nerdAnswer.guest.forEach((guestAnswer) => {
            guestCount++;
            if (guestAnswer.noeating) {
              this.pizzaCount(counter, 'invite.noeat');
            } else {
              if (guestAnswer.eatingno !== '') {
                const eatingno = this.addTopping(guestAnswer, toppings);
                this.pizzaCount(counter, eatingno);
              } else {
                this.pizzaCount(counter, 'invite.noorder');
              }
            }
          });
        }
      }
    });

    counter.stats.push({ label: 'invite.accepted', count: accepted });
    if (guestCount > 0) {
      counter.stats.push({ label: 'invite.guests', count: guestCount });
    }
    counter.stats.push({ label: 'invite.declined', count: declined });
    counter.stats.push({ label: 'invite.noanswer', count: noAnswer });
    counter.stats.push({ label: 'invite.pizzamoney', count: debtors });
    counter.drinks.sort(this.compareCountEntities);
    counter.pizza.sort(this.compareCountEntities);
    return counter;
  }

  addTopping(answer: AnswerModel | GuestModel, toppings: string[]): string {
    let toppingstring = '';
    if (answer.topping !== undefined && answer.topping !== null) {
      answer.topping.forEach(($t) => {
        toppingstring =
          (toppingstring !== '' ? toppingstring + ',' : '') + toppings[$t];
      });
    }
    return (
      answer.eatingno + (toppingstring !== '' ? ' : ' + toppingstring : '')
    );
  }

  private pizzaCount(counter: Counters, pizza: string) {
    let foundPizza = false;
    counter.pizza.forEach(
      (value: CountEntity, index: number, b: CountEntity[]) => {
        if (value.label === pizza) {
          value.count += 1;
          b[index] = value;
          foundPizza = true;
        }
      }
    );
    if (!foundPizza) {
      counter.pizza.push({ label: pizza, count: 1 });
    }
  }

  compareCountEntities(a: CountEntity, b: CountEntity) {
    if (a.label < b.label) {
      return -1;
    }
    if (a.label > b.label) {
      return +1;
    }
    return 0;
  }
}
