import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from '../../invite.state';
import { Observable, combineLatest } from 'rxjs';
import {
  selectIncludeSuspendedState,
  selectAccountingState
} from '../../store/answer/answer.selector';
import { take } from 'rxjs/operators';
import {
  answerSetAccounting,
  answerSetIncludeSuspended,
  answerLoad
} from '../../store/answer/answer.actions';
import { InvitationModel } from '../../store/invitation/invitation.model';
import { selectCurrentInvitation } from '../../store/invitation/invitation.selector';
import { Router } from '@angular/router';
import { inviteDelete } from '../../store/invitation/invitation.actions';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ConfirmDialogService } from '../../../../shared/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'ian-action-bar',
  templateUrl: './action-bar.component.html',
  styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit {
  afstemning$: Observable<boolean>;
  activeNerds$: Observable<boolean>;
  invite$: Observable<InvitationModel>;

  constructor(
    private router: Router,
    private confirmDialog: ConfirmDialogService,
    private store: Store<State>,
    private bottomSheetRef: MatBottomSheet
  ) {
    this.afstemning$ = this.store.pipe(select(selectAccountingState));
    this.activeNerds$ = this.store.pipe(select(selectIncludeSuspendedState));
    this.invite$ = this.store.pipe(select(selectCurrentInvitation));
  }

  ngOnInit() {}

  afstemning() {
    this.afstemning$.pipe(take(1)).subscribe((value) => {
      this.store.dispatch(answerSetAccounting({ enableAccounting: !value }));
    });
    this.bottomSheetRef.dismiss();
  }

  hideNerds() {
    this.activeNerds$.pipe(take(1)).subscribe((value) => {
      this.store.dispatch(
        answerSetIncludeSuspended({ includeSuspended: !value })
      );
    });
    this.bottomSheetRef.dismiss();
  }

  edit() {
    this.invite$.pipe(take(1)).subscribe((invite) => {
      console.log(invite);
      this.router.navigate(['invite/list/' + invite.id + '/edit']);
    });
    this.bottomSheetRef.dismiss();
  }

  delete() {
    combineLatest([
      this.confirmDialog
        .openConfirmDialog('Vil du slette denne invitation?')
        .pipe(take(1)),
      this.invite$.pipe(take(1))
    ]).subscribe(([confirmResult, invite]) => {
      if (confirmResult) {
        this.store.dispatch(inviteDelete({ inviteId: invite.id }));
        this.router.navigate(['invite/list']);
      }
    });
    this.bottomSheetRef.dismiss();
  }
  reload() {
    this.invite$.pipe(take(1)).subscribe((invite) => {
      this.store.dispatch(answerLoad({ inviteId: invite.id }));
    });
  }
}
