import { BottomSheetComponent } from './bottom-sheet/bottom-sheet.component';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { InvitationModel } from '../store/invitation/invitation.model';
import { map } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { select, Store } from '@ngrx/store';
import {
  selectCurrentInvitation,
  selectInviteLoadingIndicator
} from '../store/invitation/invitation.selector';
import { State } from '../invite.state';
import { DomSanitizer } from '@angular/platform-browser';
import { MatBottomSheet } from '@angular/material/bottom-sheet';

@Component({
  selector: 'ian-detailed-view',
  templateUrl: './detailed-view.component.html',
  styleUrls: ['./detailed-view.component.scss']
})
export class DetailedViewComponent implements OnInit, OnDestroy {
  invite$: Observable<InvitationModel>;
  afstemning$: Observable<boolean>;
  loading$: Observable<boolean>;

  sub: Subscription[] = [];

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(['(max-width: 768px)'])
    .pipe(map((result) => result.matches));

  constructor(
    private store: Store<State>,
    private bottomSheet: MatBottomSheet,
    private breakpointObserver: BreakpointObserver,
    private sanitizer: DomSanitizer
  ) {
    this.invite$ = this.store.pipe(select(selectCurrentInvitation));
    this.loading$ = this.store.pipe(select(selectInviteLoadingIndicator));
  }

  ngOnInit() {}

  ngOnDestroy() {
    if (this.sub !== undefined) {
      this.sub.forEach((subscriber) => subscriber.unsubscribe());
    }
  }

  showBottom() {
    this.bottomSheet.open(BottomSheetComponent);
  }

  sms(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl('sms:' + url);
  }
}
