import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService } from '../../core/core.module';
import { InviteListComponent } from './invite-list/invite-list.component';
import { InviteDetailComponent } from './invite-detail/invite-detail.component';
import { InviteComponent } from './invite/invite.component';
import { UpcommingComponent } from './upcomming/upcomming.component';
import { InviteEditComponent } from './invite-edit/invite-edit.component';

const routes: Routes = [
  {
    path: '',
    component: InviteComponent,
    children: [
      {
        path: '',
        redirectTo: 'detail/next',
        pathMatch: 'full'
      },
      {
        path: 'list',
        component: InviteListComponent,
        canActivate: [AuthGuardService],
        data: { title: 'invite.menu.listall' }
      },
      {
        path: 'detail/next',
        component: UpcommingComponent,
        canActivate: [AuthGuardService],
        data: { title: 'invite.menu.next' }
      },
      {
        path: 'list/:id',
        component: InviteDetailComponent,
        canActivate: [AuthGuardService],
        data: { title: 'invite.menu.view' }
      },
      {
        path: 'list/:id/edit',
        component: InviteEditComponent,
        canActivate: [AuthGuardService],
        data: { title: 'invite.menu.edit' }
      },
      {
        path: 'new',
        component: InviteEditComponent,
        canActivate: [AuthGuardService],
        data: { title: 'invite.menu.new' }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InviteRoutingModule {}
