import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { InvitationModel } from '../store/invitation/invitation.model';
import { select, Store } from '@ngrx/store';
import { State } from '../invite.state';
import { Observable, Subscription } from 'rxjs';
import {
  selectAllInvites,
  selectInviteLoadingIndicator
} from '../store/invitation/invitation.selector';
import { inviteLoadAll } from '../store/invitation/invitation.actions';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ian-invite-list',
  templateUrl: './invite-list.component.html',
  styleUrls: ['./invite-list.component.scss']
})
export class InviteListComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['date', 'name', 'homeFrom', 'statistics'];

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map((result) => result.matches));

  invitations$: Observable<InvitationModel[]>;
  loading$: Observable<boolean>;

  private sub: Subscription[] = [];
  constructor(
    public store: Store<State>,
    private router: Router,
    private breakpointObserver: BreakpointObserver
  ) {
    this.invitations$ = this.store.pipe(select(selectAllInvites)).pipe(
      map((invitations) => {
        return invitations.filter((i) => i.deleted !== true);
      })
    );
    this.loading$ = this.store.pipe(select(selectInviteLoadingIndicator));
  }

  ngOnInit() {
    this.store.dispatch(inviteLoadAll());
    this.sub.push(
      this.isHandset$.subscribe((handset) => {
        if (handset) {
          this.displayedColumns = ['date', 'name', 'homeFrom'];
        } else {
          this.displayedColumns = ['date', 'name', 'homeFrom', 'statistics'];
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.sub !== undefined) {
      this.sub.forEach((subscriber) => subscriber.unsubscribe());
    }
  }

  showDetail(invite: InvitationModel) {
    this.router.navigate(['invite/list/', invite.id]);
  }
}
