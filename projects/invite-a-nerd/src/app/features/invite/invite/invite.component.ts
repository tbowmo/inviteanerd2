import { Component, OnInit } from '@angular/core';
import { routeAnimations, selectAuth } from '../../../core/core.module';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import { Observable } from 'rxjs';

@Component({
  selector: 'ian-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss'],
  animations: [routeAnimations]
})
export class InviteComponent implements OnInit {
  isAuthenticated$: Observable<boolean>;

  inviteSections = [
    { link: 'detail/next', label: 'invite.menu.next', auth: true },
    { link: 'list', label: 'invite.menu.listall', auth: true },
    { link: 'new', label: 'invite.menu.new', auth: true }
  ];
  constructor(private store: Store) {}

  ngOnInit() {
    this.isAuthenticated$ = this.store.pipe(
      select(selectAuth),
      map((auth) => auth.isAuthenticated)
    );
  }
}
