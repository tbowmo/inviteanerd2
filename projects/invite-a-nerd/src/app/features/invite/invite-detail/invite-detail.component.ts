import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy
} from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../invite.state';
import { ActivatedRoute } from '@angular/router';
import { inviteLoad } from '../store/invitation/invitation.actions';

@Component({
  selector: 'ian-invite-detail',
  templateUrl: './invite-detail.component.html',
  styleUrls: ['./invite-detail.component.scss']
})
export class InviteDetailComponent implements OnInit, OnDestroy {
  sub: Subscription;
  constructor(private store: Store<State>, private route: ActivatedRoute) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.store.dispatch(inviteLoad({ inviteId: Number(params['id']) }));
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
