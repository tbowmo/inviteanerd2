import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserTestingModule } from '@angular/platform-browser/testing';
import { CoreModule } from '../../../core/core.module';
import { SettingsModule } from '../../settings/settings.module';

import { InviteDetailComponent } from './invite-detail.component';

xdescribe('InviteDetailComponent', () => {
  let component: InviteDetailComponent;
  let fixture: ComponentFixture<InviteDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserTestingModule, CoreModule, SettingsModule],
      declarations: [InviteDetailComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
