import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { InvitationModel } from '../store/invitation/invitation.model';
import { State } from '../invite.state';
import { Store, select } from '@ngrx/store';
import { inviteUpcomming } from '../store/invitation/invitation.actions';
import { selectCurrentInvitation } from '../store/invitation/invitation.selector';

@Component({
  selector: 'ian-upcomming',
  templateUrl: './upcomming.component.html',
  styleUrls: ['./upcomming.component.css']
})
export class UpcommingComponent implements OnInit {
  invite$: Observable<InvitationModel>;
  constructor(private store: Store<State>) {
    this.invite$ = this.store.pipe(select(selectCurrentInvitation));
  }

  ngOnInit() {
    this.store.dispatch(inviteUpcomming());
  }
}
