import { createAction, props } from '@ngrx/store';
import { InvitationModel } from './invitation.model';

export const inviteLoadAll = createAction('[Invitation] Load all');
export const inviteLoadAllSuccess = createAction(
  '[Invitation] Load all success',
  props<{ invitations: InvitationModel[] }>()
);
export const inviteLoad = createAction(
  '[Invitation] Load',
  props<{ inviteId: number }>()
);
export const inviteLoadSuccess = createAction(
  '[Invitation] Load success',
  props<{ invitation: InvitationModel }>()
);
export const inviteCreateOrUpdate = createAction(
  '[Invitation] create or update',
  props<{ invitation: InvitationModel }>()
);
export const inviteCreateOrUpdateSuccess = createAction(
  '[Invitation] CREATE UPDATE SUCCESS',
  props<{ invitation: InvitationModel }>()
);
export const inviteUpcomming = createAction('[Invitation] upcomming');
export const inviteUpcommintSuccess = createAction(
  '[Invitation] UPCOMMING SUCCESS'
);
export const inviteClearCurrent = createAction('[Invitation] Clear current');
export const inviteDelete = createAction(
  '[Invitation] Delete',
  props<{ inviteId: number }>()
);
export const inviteDeleteSuccess = createAction(
  '[Invitation] Delete success',
  props<{ invitation: InvitationModel }>()
);
export const inviteSetMobilepay = createAction('[Invitation] Set mobilepay');
export const inviteError = createAction('[Invitation] Error');
export const inviteSetMobilePay = createAction(
  '[Invitation] setMobilePay',
  props<{ mobilepay: string }>()
);
