import * as dayjs from 'dayjs';
import { of } from 'rxjs';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';

import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from '../../../../core/core.module';
import { State } from '../../invite.state';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import {
  inviteCreateOrUpdate,
  inviteCreateOrUpdateSuccess,
  inviteDelete,
  inviteDeleteSuccess,
  inviteLoadAllSuccess,
  inviteLoad,
  inviteLoadSuccess,
  inviteUpcomming,
  inviteLoadAll
} from './invitation.actions';
import { InvitationModel } from './invitation.model';
import { InvitationService } from './invitation.service';
import { answerLoad, answerSetAccounting } from '../answer/answer.actions';

@Injectable()
export class InvitationEffects {
  constructor(
    private actions$: Actions<Action>,
    private invitationService: InvitationService,
    private store$: Store<State>,
    private router: Router,
    private notifications: NotificationService
  ) {}

  loadAllInvitations = createEffect(() =>
    this.actions$.pipe(
      ofType(inviteLoadAll),
      switchMap(() =>
        this.invitationService.getInviteList().pipe(
          catchError((err: HttpErrorResponse) => {
            return of(null);
          })
        )
      ),
      filter((data) => data != null),
      map((data) => inviteLoadAllSuccess({ invitations: data }))
    )
  );

  loadInvitation = createEffect(() =>
    this.actions$.pipe(
      ofType(inviteLoad),
      switchMap((action) =>
        this.invitationService.getInvite(action.inviteId).pipe(
          catchError((err) => {
            if (err.status === 404) {
              this.router.navigate(['/404']);
            }
            return of(null);
          })
        )
      ),
      filter((invitation: InvitationModel) => invitation != null),
      map((invitation) => inviteLoadSuccess({ invitation }))
    )
  );

  createInvitation = createEffect(() =>
    this.actions$.pipe(
      ofType(inviteCreateOrUpdate),
      switchMap((invite) =>
        this.invitationService.updateInvite(invite.invitation).pipe(
          catchError((_err: HttpErrorResponse) => {
            return of(null);
          })
        )
      ),
      filter((data) => data !== null),
      map((updatedInvite: InvitationModel) =>
        inviteCreateOrUpdateSuccess({ invitation: updatedInvite })
      )
    )
  );

  createInviteSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(inviteCreateOrUpdateSuccess),
        tap((action) => {
          this.router.navigate(['invite/list/' + action.invitation.id]);
        })
      ),
    { dispatch: false }
  );

  upcommingEvent = createEffect(() =>
    this.actions$.pipe(
      ofType(inviteUpcomming),
      switchMap(() =>
        this.invitationService.getFirstUpcommingInvite().pipe(
          catchError((err: HttpErrorResponse) => {
            return of(null);
          })
        )
      ),
      filter((data) => data !== undefined && data !== null),
      map((invitation: InvitationModel) => inviteLoadSuccess({ invitation }))
    )
  );

  loadSingleSuccess = createEffect(() =>
    this.actions$.pipe(
      ofType(inviteLoadSuccess),
      switchMap(({ invitation }) => {
        console.log({ invitation });
        const enableAccounting: boolean =
          dayjs(invitation?.eventstart)
            .startOf('day')
            .add(20, 'hour')
            .toDate()
            .getTime() < Date.now();
        return [
          answerLoad({ inviteId: invitation.id }),
          answerSetAccounting({ enableAccounting })
        ];
      })
    )
  );

  deleteInvite = createEffect(() =>
    this.actions$.pipe(
      ofType(inviteDelete),
      switchMap((inviteId) =>
        this.invitationService
          .deleteInvite(inviteId.inviteId)
          .pipe<InvitationModel>(
            catchError((err: HttpErrorResponse) => {
              this.notifications.error('Delete failed : ' + err.statusText);
              return of(null);
            })
          )
      ),
      filter((invite) => invite !== null),
      tap((data) => {
        if (data) this.notifications.success('Slettet invitation');
      }),
      map((invitation) => inviteDeleteSuccess({ invitation }))
    )
  );
}
