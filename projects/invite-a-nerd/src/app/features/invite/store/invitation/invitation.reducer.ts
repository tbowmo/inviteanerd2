import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';
import { InvitationModel } from '../../store/invitation/invitation.model';
import {
  inviteClearCurrent,
  inviteCreateOrUpdateSuccess,
  inviteDeleteSuccess,
  inviteLoad,
  inviteLoadAll,
  inviteLoadAllSuccess,
  inviteLoadSuccess,
  inviteUpcomming
} from './invitation.actions';

export const invitationsAdapter = createEntityAdapter<InvitationModel>({
  selectId: (invite: InvitationModel) => invite.id,
  sortComparer: false
});

export interface InviteState extends EntityState<InvitationModel> {
  currentInvitation?: InvitationModel;
  loaded?: boolean;
  loading?: boolean;
}

export const INIT_STATE: InviteState = invitationsAdapter.getInitialState({
  currentInvitation: undefined,
  loaded: undefined,
  loading: false
});

const reducer = createReducer(
  INIT_STATE,
  on(inviteLoad, (state) => ({ ...state, loading: true })),
  on(inviteLoadAll, (state) => ({ ...state, loading: true })),
  on(inviteLoadSuccess, (state, { invitation }) => ({ ...state })),
  on(inviteClearCurrent, (state) => ({
    ...state,
    currentIvitation: undefined
  })),
  on(inviteUpcomming, (state) => ({ ...state, currentInvitation: undefined })),
  on(inviteLoadAllSuccess, (state, { invitations }) =>
    invitationsAdapter.setAll(invitations, {
      ...state,
      loaded: true,
      loading: false
    })
  ),
  on(inviteLoadSuccess, (state, { invitation }) => ({
    ...state,
    currentInvitation: invitation,
    loading: false
  })),
  on(inviteCreateOrUpdateSuccess, (state, { invitation }) =>
    invitationsAdapter.upsertOne(invitation, {
      ...state,
      currentInvitation: invitation
    })
  ),
  on(inviteDeleteSuccess, (state, { invitation }) =>
    invitationsAdapter.removeOne(invitation.id, {
      ...state,
      currentInvitation: undefined
    })
  )
);

export function inviteReducer(state: InviteState, action: Action): InviteState {
  return reducer(state, action);
}
