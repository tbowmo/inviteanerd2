import {
  NerdModel,
  NerdsiteModel,
  RestaurantModel
} from '../../../../shared-nerd';

export interface InvitationModel {
  contribution: number;
  deleted?: boolean;
  editable?: boolean;
  eventstart: Date;
  hostid: number;
  id: number;
  information: string;
  mobilepay?: string;
  mobilepaynerd?: NerdModel;
  mobilepaynerdid?: number;
  nerd?: NerdModel;
  nerdsite?: NerdsiteModel;
  nerdsiteid: number;
  restaurant?: RestaurantModel;
  restaurantid: number;
}
