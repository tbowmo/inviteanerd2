import * as dayjs from 'dayjs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { InvitationModel } from './invitation.model';
import { environment } from '../../../../../environments/environment';

@Injectable()
export class InvitationService {
  constructor(private http: HttpClient) {}

  getInvite(inviteId: number): Observable<InvitationModel> {
    return this.http
      .get<InvitationModel>(environment.backend + `invite/${inviteId}`)
      .pipe<InvitationModel>(map((response) => this.conditioner(response)));
  }

  getInviteList(): Observable<InvitationModel[]> {
    return this.http
      .get<InvitationModel[]>(environment.backend + `invite/list`)
      .pipe<InvitationModel[]>(
        map((response) => response.map((e) => this.conditioner(e)))
      );
  }

  updateInvite(invite: InvitationModel): Observable<InvitationModel> {
    return this.http
      .post<InvitationModel>(environment.backend + 'invite', invite)
      .pipe<InvitationModel>(map((response) => this.conditioner(response)));
  }

  getFirstUpcommingInvite(): Observable<InvitationModel> {
    return this.http
      .get<InvitationModel>(environment.backend + 'invite/next')
      .pipe<InvitationModel>(map((result) => this.conditioner(result)));
  }

  deleteInvite(id: number): Observable<InvitationModel> {
    return this.http
      .delete<InvitationModel>(environment.backend + 'invite/' + id)
      .pipe<InvitationModel>(map((result) => this.conditioner(result)));
  }

  private conditioner(data: InvitationModel): InvitationModel {
    const eventStart = new Date(data.eventstart);
    const startOfDay = dayjs().startOf('day').toDate();
    const editable = !data.deleted && eventStart > startOfDay;
    return {
      ...data,
      eventstart: eventStart,
      editable: editable
    };
  }
}
