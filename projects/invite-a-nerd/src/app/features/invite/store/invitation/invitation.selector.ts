import { createSelector } from '@ngrx/store';

import { selectRouterState } from '../../../../core/core.state';
import { InviteState, selectInvite } from '../../invite.state';

import { invitationsAdapter } from './invitation.reducer';

const { selectEntities, selectAll } = invitationsAdapter.getSelectors();

const selectInvitations = createSelector(
  selectInvite,
  (state: InviteState) => state.invitation
);

export const selectAllInvites = createSelector(selectInvitations, selectAll);
export const selectInvitationEntities = createSelector(
  selectInvitations,
  selectEntities
);

export const selectSelectedInvite = createSelector(
  selectInvitationEntities,
  selectRouterState,
  (entities, params) => params && entities[params.state.params.id]
);

export const selectCurrentInvitation = createSelector(
  selectInvitations,
  (state) => state.currentInvitation
);

export const selectInviteLoadingIndicator = createSelector(
  selectInvitations,
  (state) => state.loading
);
