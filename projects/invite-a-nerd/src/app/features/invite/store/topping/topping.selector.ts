import { createSelector } from '@ngrx/store';

import { InviteState, selectInvite } from '../../invite.state';

import { toppingAdapter } from './topping.reducer';

const { selectEntities, selectAll } = toppingAdapter.getSelectors();

const selectToppings = createSelector(
  selectInvite,
  (state: InviteState) => state.topping
);

export const selectAllToppings = createSelector(selectToppings, selectAll);
export const selectToppingEntities = createSelector(
  selectToppings,
  selectEntities
);
