import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ToppingModel } from './topping.model';
import { map } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';

@Injectable()
export class ToppingService {
  constructor(private http: HttpClient) {}

  getToppings(): Observable<ToppingModel[]> {
    return this.http.get<ToppingModel[]>(environment.backend + `topping`).pipe(
      map((response) =>
        response.map((e) => ({
          ...e,
          id: Number(e.id)
        }))
      )
    );
  }

  update(topping: ToppingModel): Observable<ToppingModel> {
    return this.http
      .post<ToppingModel>(environment.backend + 'topping', topping)
      .pipe(
        map((response) => ({
          ...response,
          id: Number(response.id)
        }))
      );
  }
}
