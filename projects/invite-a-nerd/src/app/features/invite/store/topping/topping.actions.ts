import { createAction, props } from '@ngrx/store';
import { ToppingModel } from './topping.model';

export const toppingLoad = createAction('[Toping] Load');
export const toppingLoadSuccess = createAction(
  '[Toping] load success',
  props<{ toppings: ToppingModel[] }>()
);
export const toppingUpdate = createAction(
  '[Topping] update',
  props<{ topping: ToppingModel }>()
);
export const toppingUpdateSuccess = createAction(
  '[Topping] update success',
  props<{ topping: ToppingModel }>()
);
export const toppingError = createAction('[Topping] error');
