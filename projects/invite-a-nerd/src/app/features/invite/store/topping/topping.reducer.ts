import { createEntityAdapter, EntityState, Update } from '@ngrx/entity';
import { ToppingModel, ToppingState } from './topping.model';
import { toppingLoadSuccess, toppingUpdateSuccess } from './topping.actions';
import { createReducer, Action, on } from '@ngrx/store';

export const toppingAdapter = createEntityAdapter<ToppingModel>({
  selectId: (topping: ToppingModel) => topping.id,
  sortComparer: false
});

export interface State extends EntityState<ToppingModel> {
  loaded?: boolean;
}

export const INIT_STATE: State = toppingAdapter.getInitialState({
  loaded: false
});

const reducer = createReducer(
  INIT_STATE,
  on(toppingLoadSuccess, (state, { toppings }) => {
    toppingAdapter.removeAll(state);
    return toppingAdapter.setAll(toppings, state);
  }),
  on(toppingUpdateSuccess, (state, { topping }) => {
    const updateTopping: Update<ToppingModel> = {
      id: topping.id,
      changes: topping
    };
    return toppingAdapter.updateOne(updateTopping, state);
  })
);

export function toppingReducer(
  state: ToppingState,
  action: Action
): ToppingState {
  return reducer(state, action);
}
