import { Action, Store } from '@ngrx/store';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { ToppingModel } from './topping.model';

import {
  map,
  switchMap,
  withLatestFrom,
  filter,
  catchError,
  tap
} from 'rxjs/operators';
import { of } from 'rxjs';
import { ToppingService } from './topping.service';
import { State } from '../../invite.state';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from '../../../../core/core.module';
import {
  toppingError,
  toppingLoad,
  toppingLoadSuccess,
  toppingUpdate,
  toppingUpdateSuccess
} from './topping.actions';

@Injectable()
export class ToppingEffects {
  constructor(
    private actions$: Actions<Action>,
    private toppingService: ToppingService,
    private store$: Store<State>,
    private notifications: NotificationService
  ) {}

  loadAllToppings = createEffect(() =>
    this.actions$.pipe(
      ofType(toppingLoad),
      withLatestFrom(this.store$),
      filter(([_action, storeState]) => {
        return !storeState.invite.topping.loaded;
      }),
      switchMap(() =>
        this.toppingService.getToppings().pipe(
          catchError((_err: HttpErrorResponse) => {
            console.log(_err.statusText);
            return null;
          })
        )
      ),
      map((toppings: ToppingModel[]) => {
        if (toppings === null) {
          return toppingError();
        } else {
          return toppingLoadSuccess({ toppings });
        }
      })
    )
  );

  updateTopping = createEffect(() =>
    this.actions$.pipe(
      ofType(toppingUpdate),
      switchMap((topping) =>
        this.toppingService.update(topping.topping).pipe(
          catchError((_err: HttpErrorResponse) => {
            console.log(_err.statusText);
            return of(null);
          })
        )
      ),
      filter((data) => !!data),
      map((updatedTopping: ToppingModel) =>
        toppingUpdateSuccess({ topping: updatedTopping })
      )
    )
  );

  updateToppingSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(toppingUpdateSuccess),
        tap((topping) => {
          if (topping) {
            this.notifications.success('Updated toppings');
          }
        })
      ),
    { dispatch: false }
  );
}
