import { EntityState } from '@ngrx/entity';

export interface ToppingModel {
  id: number;
  name: string;
}

export interface ToppingState extends EntityState<ToppingModel> {}
