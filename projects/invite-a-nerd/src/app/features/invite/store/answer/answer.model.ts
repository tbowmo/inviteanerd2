import { NerdModel } from '../../../../shared-nerd';

interface BaseAnswerModel {
  nerdid: number;
  invitationid: number;
  eatingno?: string;
  price?: number;
  haspaid?: boolean;
  noeating?: boolean;
  topping?: number[];
}

export interface AnswerModel extends BaseAnswerModel {
  nerd?: NerdModel;
  attending: AnswerAttending;
  guest?: GuestModel[];
}

export interface GuestModel extends BaseAnswerModel {
  id: number;
  name: string;
}

export enum AnswerAttending {
  NO_ANSWER = 0,
  DECLINED = 1,
  ACCEPTED = 2
}
