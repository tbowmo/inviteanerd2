import { createAction, props } from '@ngrx/store';
import { AnswerModel, GuestModel } from './answer.model';

export const answerLoad = createAction(
  '[Answer] LOAD ALL',
  props<{ inviteId: number }>()
);
export const answerUpdate = createAction(
  '[Answer] Update',
  props<{ answer: AnswerModel }>()
);
export const answerLoadSuccess = createAction(
  '[Answer] Load all success',
  props<{ answers: AnswerModel[] }>()
);
export const answerUpdateSuccess = createAction(
  '[Answer] Update success',
  props<{ answer: AnswerModel }>()
);
export const answerSetAccounting = createAction(
  '[Answer] SetAccounting',
  props<{ enableAccounting: boolean }>()
);
export const answerSetIncludeSuspended = createAction(
  '[Answer] setIncludeSuspended',
  props<{ includeSuspended: boolean }>()
);
export const answerUpdateGuest = createAction(
  '[Answer] UpdateGuestAnswer',
  props<{ guest: GuestModel }>()
);
export const answerDeleteGuest = createAction(
  '[Answer] deleteGuestAnswer',
  props<{ guest: GuestModel }>()
);
export const answerError = createAction('[Answer] error');
