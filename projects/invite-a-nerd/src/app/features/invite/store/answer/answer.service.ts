import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AnswerModel, AnswerAttending, GuestModel } from './answer.model';
import { map } from 'rxjs/operators';
import { InvitationModel } from '../invitation/invitation.model';
import { environment } from '../../../../../environments/environment';

@Injectable()
export class AnswerService {
  constructor(private http: HttpClient) {}

  getAnswers(
    id: number,
    includeSuspended: boolean = false
  ): Observable<AnswerModel[]> {
    return this.http
      .get<AnswerModel[]>(environment.backend + `answer/${id}`)
      .pipe(
        map((answer: AnswerModel[]) => {
          if (includeSuspended) {
            return answer;
          }
          return answer.filter(
            (a) =>
              !(a.nerd.suspended && a.attending === AnswerAttending.NO_ANSWER)
          );
        })
      );
  }

  update(answer: AnswerModel): Observable<AnswerModel> {
    return this.http.post<AnswerModel>(
      environment.backend + 'answer/update',
      answer
    );
  }

  pizzaHistory(
    answer: AnswerModel,
    invitation: InvitationModel
  ): Observable<string[]> {
    return this.http.get<string[]>(
      environment.backend +
        `answer/pizzahistory/${answer.nerdid}/${invitation.restaurantid}`
    );
  }

  updateGuest(guest: GuestModel): Observable<AnswerModel> {
    return this.http.post<AnswerModel>(
      environment.backend + 'answer/addOrUpdateGuest',
      guest
    );
  }

  deleteGuest(guest: GuestModel): Observable<AnswerModel> {
    return this.http.post<AnswerModel>(
      environment.backend + 'answer/deleteGuest',
      guest
    );
  }
}
