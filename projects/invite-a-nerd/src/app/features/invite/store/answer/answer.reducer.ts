import { createEntityAdapter, EntityState, Update } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';
import {
  answerLoad,
  answerLoadSuccess,
  answerSetAccounting,
  answerSetIncludeSuspended,
  answerUpdate,
  answerUpdateSuccess
} from './answer.actions';
import { AnswerModel } from './answer.model';

export const answersAdapter = createEntityAdapter<AnswerModel>({
  selectId: (invite: AnswerModel) => invite.nerdid,
  sortComparer: false
});

export interface AnswerState extends EntityState<AnswerModel> {
  accounting: boolean;
  includeSuspendedNerds: boolean;
  loading: boolean;
  updating: boolean;
}

export const INIT_STATE: AnswerState = answersAdapter.getInitialState({
  accounting: false,
  includeSuspendedNerds: false,
  loading: false,
  updating: false
});

const reducer = createReducer(
  INIT_STATE,
  on(answerLoad, (state) => ({ ...state, loading: true })),
  on(answerLoadSuccess, (state, { answers }) => {
    answersAdapter.removeAll(state);
    return answersAdapter.setAll(answers, {
      ...state,
      loading: false
    });
  }),
  on(answerUpdate, (state) => ({ ...state, updating: true })),
  on(answerUpdateSuccess, (state, { answer }) => {
    const updateAnswer: Update<AnswerModel> = {
      changes: answer,
      id: answer.nerdid
    };
    return answersAdapter.updateOne(updateAnswer, {
      ...state,
      updating: false
    });
  }),
  on(answerSetAccounting, (state, { enableAccounting }) => ({
    ...state,
    accounting: enableAccounting
  })),
  on(answerSetIncludeSuspended, (state, { includeSuspended }) => ({
    ...state,
    includeSuspendedNerds: includeSuspended
  }))
);

export function answerReducer(state: AnswerState, action: Action): AnswerState {
  return reducer(state, action);
}
