import { createSelector } from '@ngrx/store';

import { InviteState, selectInvite } from '../../invite.state';

import { answersAdapter } from './answer.reducer';

const { selectEntities, selectAll } = answersAdapter.getSelectors();

const selectAnswers = createSelector(
  selectInvite,
  (state: InviteState) => state.answer
);

export const selectAllAnswers = createSelector(selectAnswers, selectAll);
export const selectAnswerEntities = createSelector(
  selectAnswers,
  selectEntities
);

export const selectAccountingState = createSelector(
  selectAnswers,
  (state) => state.accounting
);

export const selectIncludeSuspendedState = createSelector(
  selectAnswers,
  (state) => state.includeSuspendedNerds
);

export const selectAnswersLoadingIndicator = createSelector(
  selectAnswers,
  (state) => state.loading
);

export const selectUpdateInProgress = createSelector(
  selectAnswers,
  (state) => state.updating
);

export const selectSingleAnswer = createSelector(
  selectAnswerEntities,
  (state, props) => props && state[props.id]
);
