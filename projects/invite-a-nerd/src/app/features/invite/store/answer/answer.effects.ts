import { Action, Store } from '@ngrx/store';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { AnswerModel } from './answer.model';

import {
  map,
  switchMap,
  catchError,
  filter,
  tap,
  withLatestFrom
} from 'rxjs/operators';
import { of } from 'rxjs';
import { AnswerService } from './answer.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from '../../../../core/core.module';
import { State } from '../../invite.state';
import {
  answerLoad,
  answerUpdate,
  answerUpdateSuccess,
  answerLoadSuccess,
  answerSetIncludeSuspended,
  answerUpdateGuest,
  answerDeleteGuest,
  answerError
} from './answer.actions';
@Injectable()
export class AnswerEffects {
  constructor(
    private actions$: Actions<Action>,
    private answerService: AnswerService,
    private notifications: NotificationService,
    private store$: Store<State>
  ) {}

  loadAllAnswers = createEffect(() =>
    this.actions$.pipe(
      ofType(answerLoad),
      withLatestFrom(this.store$),
      switchMap(([action, store$]) =>
        this.answerService
          .getAnswers(
            action.inviteId,
            store$.invite.answer.includeSuspendedNerds
          )
          .pipe(
            catchError((_err: HttpErrorResponse) => {
              return of(null);
            })
          )
      ),
      filter((data) => data !== null),
      map((answers: AnswerModel[]) => answerLoadSuccess({ answers }))
    )
  );

  updateAnswer = createEffect(() =>
    this.actions$.pipe(
      ofType(answerUpdate),
      map((action) => action.answer),
      switchMap((answer: AnswerModel) =>
        this.answerService.update(answer).pipe(
          catchError((err: HttpErrorResponse) => {
            return of(null);
          })
        )
      ),
      map((answer: AnswerModel) => {
        if (answer) {
          return answerUpdateSuccess({ answer });
        }
        return answerError();
      })
    )
  );

  notifyUpdate = createEffect(
    () =>
      this.actions$.pipe(
        ofType(answerUpdateSuccess),
        tap((answer) => {
          this.notifications.success(
            'Opdateret svar for ' + answer.answer.nerd.name
          );
        })
      ),
    {
      dispatch: false
    }
  );

  getAnswers = createEffect(() =>
    this.actions$.pipe(
      ofType(answerSetIncludeSuspended),
      withLatestFrom(this.store$),
      map(([_value, store]) =>
        answerLoad({ inviteId: store.invite.invitation.currentInvitation.id })
      )
    )
  );

  addGuest = createEffect(() =>
    this.actions$.pipe(
      ofType(answerUpdateGuest),
      switchMap((guest) =>
        this.answerService
          .updateGuest(guest.guest)
          .pipe(catchError((_e) => of(null)))
      ),
      filter((data) => data != null),
      map((answer: AnswerModel) =>
        answerUpdateSuccess({
          answer
        })
      )
    )
  );

  deleteGuest = createEffect(() =>
    this.actions$.pipe(
      ofType(answerDeleteGuest),
      switchMap((guest) =>
        this.answerService
          .deleteGuest(guest.guest)
          .pipe(catchError((_e) => of(null)))
      ),
      filter((data) => data != null),
      map((answer: AnswerModel) =>
        answerUpdateSuccess({
          answer
        })
      )
    )
  );
}
