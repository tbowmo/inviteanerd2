import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteEditComponent } from './invite-edit.component';

xdescribe('InviteEditComponent', () => {
  let component: InviteEditComponent;
  let fixture: ComponentFixture<InviteEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InviteEditComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
