import * as dayjs from 'dayjs';
import { Observable, of, Subscription } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  startWith,
  switchMap,
  take
} from 'rxjs/operators';

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';

import { State } from '../invite.state';
import {
  inviteClearCurrent,
  inviteCreateOrUpdate,
  inviteLoad
} from '../store/invitation/invitation.actions';
import { InvitationModel } from '../store/invitation/invitation.model';
import { selectCurrentInvitation } from '../store/invitation/invitation.selector';
import {
  NerdModel,
  NerdsiteModel,
  RestaurantModel,
  selectAllNerdsites,
  selectAllRestaurants,
  restaurantLoad,
  siteLoad,
  lastAddedRestaurant,
  lastAddedNerdSite,
  siteUpdate,
  restaurantUpdate
} from '../../../shared-nerd';
import { MatStepper } from '@angular/material/stepper';
import { nerdLoadAll } from '../../../shared-nerd/nerd/nerd.actions';
import { selectAllNerds } from '../../../shared-nerd/nerd/nerd.selector';
import { AddressEditService } from '../../../shared/addresss-edit/address-edit.service';

@Component({
  selector: 'ian-invite-edit',
  templateUrl: './invite-edit.component.html',
  styleUrls: ['./invite-edit.component.scss']
})
export class InviteEditComponent implements OnInit, OnDestroy {
  inviteForm: FormGroup;
  nerds$: Observable<NerdModel[]>;
  filteredNerds: Observable<NerdModel[]>;
  information = '';
  stepperIndex = 0;
  disabled = false;
  inviteId = 0;
  @ViewChild('stepper') stepper: MatStepper;

  get formArray(): AbstractControl | null {
    return this.inviteForm.get('formArray');
  }

  nerdSites$: Observable<NerdsiteModel[]>;
  restaurants$: Observable<RestaurantModel[]>;
  private sub: Subscription[] = [];
  minDate = Date.now();

  constructor(
    private store: Store<State>,
    private _formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private addressEditService: AddressEditService
  ) {
    this.nerds$ = this.store.pipe(select(selectAllNerds));
    this.nerdSites$ = this.store.pipe(select(selectAllNerdsites));
    this.restaurants$ = this.store.pipe(select(selectAllRestaurants));
  }

  ngOnDestroy() {
    if (this.sub !== undefined) {
      this.sub.forEach((subscriber) => subscriber.unsubscribe());
    }
  }

  ngOnInit() {
    if (this.router.url.endsWith('new')) {
      this.store.dispatch(inviteClearCurrent());
    } else {
      this.route.params.pipe(take(1)).subscribe((params: { id: number }) => {
        this.store.dispatch(inviteLoad({ inviteId: params.id }));
      });
    }
    this.inviteForm = this._formBuilder.group({
      formArray: this._formBuilder.array([
        this._formBuilder.group({
          nerd: ['', [Validators.required, this.validateSelection]]
        }),
        this._formBuilder.group({
          nerdsite: ['', [Validators.required, this.validateSelection]]
        }),
        this._formBuilder.group({
          norestaurant: [false],
          restaurant: ['', [Validators.required, this.validateSelection]]
        }),
        this._formBuilder.group({
          date: ['', [Validators.required, this.validateDate]],
          homeFrom: ['', Validators.required],
          information: ['', Validators.required],
          contribution: [0]
        })
      ])
    });

    this.sub.push(
      this.formArray.get('3.information').valueChanges.subscribe((value) => {
        this.information = value;
      })
    );
    this.sub.push(
      this.formArray.get('0.nerd').valueChanges.subscribe((value) => {
        if (typeof value !== 'string') {
          this.store.dispatch(siteLoad({ nerdId: (value as NerdModel).id }));
          this.nextStep(value);
        }
      })
    );

    this.sub.push(
      this.formArray.get('1.nerdsite').valueChanges.subscribe((value) => {
        if (typeof value !== 'string') {
          const nerdSite = this.formArray.get('1.nerdsite')
            .value as NerdsiteModel;
          this.store.dispatch(
            restaurantLoad({ nerdsiteId: nerdSite.id, distance: 10 })
          );
          this.nextStep(value);
        }
      })
    );

    this.sub.push(
      this.formArray.get('2.restaurant').valueChanges.subscribe((value) => {
        this.nextStep(value);
      })
    );

    this.sub.push(
      this.formArray.get('2.norestaurant').valueChanges.subscribe((value) => {
        if (value) {
          this.formArray.get('2.restaurant').disable();
        } else {
          this.formArray.get('2.restaurant').enable();
        }
        this.nextStep(value);
      })
    );
    this.sub.push(
      this.store
        .pipe(select(lastAddedNerdSite))
        .subscribe((site: NerdsiteModel) => {
          if (site !== undefined) {
            this.formArray.get('1.nerdsite').setValue(site);
          }
        })
    );

    this.sub.push(
      this.store
        .pipe(select(lastAddedRestaurant))
        .subscribe((rest: RestaurantModel) => {
          if (rest !== undefined) {
            this.formArray.get('2.restaurant').setValue(rest);
          }
        })
    );

    this.store.dispatch(nerdLoadAll());
    this.filteredNerds = this.formArray.get('0.nerd').valueChanges.pipe(
      startWith(''),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((nerd: string) => this.filterNerds(nerd))
    );

    this.loadExistingInvite();
  }

  private loadExistingInvite() {
    this.sub.push(
      this.store
        .pipe(select(selectCurrentInvitation))
        .pipe(filter((data) => data !== null))
        .subscribe((invite) => {
          if (invite !== undefined) {
            this.inviteId = invite.id;
            if (invite.eventstart < dayjs().startOf('day').toDate()) {
              this.disabled = true;
              return;
            }
            this.nerds$.pipe(take(1)).subscribe((_nerds) => {
              this.formArray.get('0.nerd').setValue(invite.nerd, {});
            });
            this.nerdSites$.pipe(take(1)).subscribe((_sites) => {
              this.formArray.get('1.nerdsite').setValue(invite.nerdsite);
            });
            this.restaurants$.pipe(take(1)).subscribe((_restaurants) => {
              if (invite.restaurant === undefined) {
                this.formArray.get('2.norestaurant').setValue(true);
              } else {
                this.formArray.get('2.restaurant').setValue(invite.restaurant);
              }
            });
            this.formArray.get('3').patchValue({
              contribution: invite.contribution,
              information: invite.information,
              date: invite.eventstart,
              homeFrom: dayjs(invite.eventstart).format('HH:mm')
            });
          }
        })
    );
  }

  private nextStep(value: any) {
    if (value === true || value.id !== undefined) {
      setTimeout(() => {
        this.stepper.next();
      }, 1000);
    }
  }

  private validateSelection(
    control: AbstractControl
  ): { [key: string]: any } | null {
    const valid = control.value.id !== undefined;

    return valid
      ? null
      : { invalidEntry: { valid: false, value: control.value } };
  }

  private validateDate(
    control: AbstractControl
  ): { [key: string]: any } | null {
    const valid = control.value >= dayjs().startOf('day').toDate();

    return valid
      ? null
      : { invalidDate: { valid: false, value: control.value } };
  }

  private filterNerds(value: string): Observable<NerdModel[]> {
    if (typeof value !== 'string') {
      value = (value as NerdModel).name;
    }
    const filterValue = value.toLocaleLowerCase();
    return this.nerds$.pipe(
      map((nerds) =>
        nerds.filter((nerd) =>
          nerd.name.toLocaleLowerCase().includes(filterValue)
        )
      )
    );
  }

  onSubmit() {
    if (this.inviteForm.invalid) {
      return;
    }
    const value = this.inviteForm.value.formArray;
    let start = dayjs(value[3]['date']).startOf('day');
    const time = (value[3]['homeFrom'] as string).split(':');
    start = start.add(Number(time[0]), 'hour').add(Number(time[1]), 'minute');
    let restaurantid = null;
    if (!this.formArray.get('2.norestaurant').value) {
      restaurantid = (value[2]['restaurant'] as RestaurantModel).id;
    }
    const invitation: InvitationModel = {
      hostid: (value[0]['nerd'] as NerdModel).id,
      eventstart: start.toDate(),
      information: value[3]['information'],
      nerdsiteid: (value[1]['nerdsite'] as NerdsiteModel).id,
      restaurantid: restaurantid,
      id: this.inviteId,
      contribution: 0
    };
    this.store.dispatch(inviteCreateOrUpdate({ invitation }));
  }

  displayFn(nerd?: NerdModel): string | undefined {
    return nerd ? nerd.name : undefined;
  }

  addNerdSite() {
    const n: NerdsiteModel = {
      deleted: false,
      address: '',
      name: '',
      nerdid: Number(this.formArray.get('0.nerd').value.id),
      id: 0,
      x: 0,
      y: 0
    };
    this.addressEditService
      .editSite(n)
      .pipe(take(1))
      .subscribe((address) => {
        if (address !== undefined) {
          this.store.dispatch(siteUpdate({ nerdSite: address }));
        }
      });
  }

  addRestaurant() {
    const n: RestaurantModel = {
      deleted: false,
      address: '',
      name: '',
      phone: '',
      link: '',
      id: 0,
      x: 0,
      y: 0
    };
    this.addressEditService
      .editSite(n)
      .pipe(take(1))
      .subscribe((address) => {
        if (address !== undefined) {
          this.store.dispatch(restaurantUpdate({ restaurant: address }));
        }
      });
  }
}
