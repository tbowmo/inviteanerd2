import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NerdComponent } from './nerd/nerd.component';
import { NerdsListComponent } from './nerds-list/nerds-list.component';
import { NerdEditComponent } from './nerd-edit/nerd-edit.component';
import { AuthGuardService } from '../../core/core.module';

const routes: Routes = [
  {
    path: '',
    component: NerdComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        canActivate: [AuthGuardService],
        component: NerdsListComponent
      },
      {
        path: 'list/:id',
        canActivate: [AuthGuardService],
        component: NerdEditComponent
      },
      {
        path: 'new',
        component: NerdEditComponent,
        canActivate: [AuthGuardService],
        data: { title: 'nerd.menu.new' }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NerdsRoutingModule {}
