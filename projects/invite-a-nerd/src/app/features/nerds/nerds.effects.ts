import { Injectable } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { merge } from 'rxjs';
import { tap, map, distinctUntilChanged, filter } from 'rxjs/operators';

import { selectSettingsLanguage, TitleService } from '../../core/core.module';
import { AppState } from '../../core/core.module';
import { actionSettingsChangeLanguage } from '../../core/settings/settings.actions';

@Injectable()
export class NerdsEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private translateService: TranslateService,
    private router: Router,
    private titleService: TitleService
  ) {}

  @Effect({ dispatch: false })
  setTranslateServiceLanguage = this.store.pipe(
    select(selectSettingsLanguage),
    distinctUntilChanged(),
    tap((language) => this.translateService.use(language))
  );

  @Effect({ dispatch: false })
  setTitle = merge(
    this.actions$.pipe(ofType(actionSettingsChangeLanguage)),
    this.router.events.pipe(filter((event) => event instanceof ActivationEnd))
  ).pipe(
    tap(() => {
      this.titleService.setTitle(
        this.router.routerState.snapshot.root,
        this.translateService
      );
    })
  );
}
