import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserTestingModule } from '@angular/platform-browser/testing';

import { NerdEditComponent } from './nerd-edit.component';

describe('NerdEditComponent', () => {
  let component: NerdEditComponent;
  let fixture: ComponentFixture<NerdEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserTestingModule],
      declarations: [NerdEditComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NerdEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
