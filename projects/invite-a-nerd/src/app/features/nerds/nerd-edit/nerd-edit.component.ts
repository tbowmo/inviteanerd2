import { ImageCroppedEvent } from 'ngx-image-cropper';
import { Observable, Subscription } from 'rxjs';
import { filter, take } from 'rxjs/operators';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  NerdModel,
  siteUpdate,
  NerdsiteModel,
  siteLoad,
  restaurantUpdate,
  restaurantLoad,
  RestaurantModel,
  selectAllNerdsites,
  selectAllRestaurants,
  State
} from '../../../shared-nerd';
import {
  nerdUpdate,
  nerdClearCurrent,
  nerdLoadSingle
} from '../../../shared-nerd/nerd/nerd.actions';
import { selectCurrentNerd } from '../../../shared-nerd/nerd/nerd.selector';
import { select, Store } from '@ngrx/store';
import { AddressEditService } from '../../../shared/addresss-edit/address-edit.service';

@Component({
  selector: 'ian-nerd-edit',
  templateUrl: './nerd-edit.component.html',
  styleUrls: ['./nerd-edit.component.scss']
})
export class NerdEditComponent implements OnInit, OnDestroy {
  nerd$: Observable<NerdModel>;
  restaurants$: Observable<RestaurantModel[]>;
  nerdSites$: Observable<NerdsiteModel[]>;
  nerdForm: FormGroup;
  editInPlace = false;
  imageChangedEvent: any = '';
  croppedImage: string | undefined = undefined;
  displayedColumns: string[] = ['name', 'address', 'action'];
  displayedColumnsRestaurant: string[] = [
    'name',
    'address',
    'distance',
    'action'
  ];

  sub: Subscription[] = [];
  constructor(
    private store: Store<State>,
    private formbuilder: FormBuilder,
    private addressEditService: AddressEditService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.nerd$ = this.store.pipe(select(selectCurrentNerd));
    this.nerdSites$ = this.store.pipe(select(selectAllNerdsites));
    this.restaurants$ = this.store.pipe(select(selectAllRestaurants));
  }

  ngOnInit() {
    if (this.router.url.endsWith('new')) {
      this.store.dispatch(nerdClearCurrent());
    } else {
      this.route.params.pipe(take(1)).subscribe((params: { id: number }) => {
        this.store.dispatch(nerdLoadSingle({ nerdId: params.id }));
      });
    }
    this.nerdForm = this.formbuilder.group({
      id: [0],
      name: ['', [Validators.required]],
      phone: ['', [Validators.pattern('[0-9]*'), Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      homepageurl: [''],
      frontpage: [false],
      suspended: [false],
      donoteatpizza: [false]
    });
    this.sub.push(
      this.nerd$
        .pipe(filter((nerd) => nerd !== undefined))
        .subscribe((nerd) => {
          this.store.dispatch(siteLoad({ nerdId: nerd.id, deleted: true }));
          this.editInPlace = false;
        })
    );
  }

  ngOnDestroy() {
    this.sub.forEach((subscription) => subscription.unsubscribe());
  }

  onSubmit() {
    let imageData = this.croppedImage;
    if (imageData !== '--') {
      imageData = imageData ? imageData.split('base64,')[1] : undefined;
    }
    const nerd: NerdModel = {
      ...this.nerdForm.value,
      imageData: imageData
    };

    this.store.dispatch(nerdUpdate({ nerd }));
  }

  showRestaurantsForNerdsite(nerdsite: NerdsiteModel) {
    this.store.dispatch(
      restaurantLoad({ nerdsiteId: nerdsite.id, distance: 10 })
    );
  }

  nerdsite(site: NerdsiteModel) {
    this.sub.push(
      this.addressEditService.editSite(site).subscribe((address) => {
        if (address) {
          this.store.dispatch(siteUpdate({ nerdSite: address }));
        }
      })
    );
  }

  restaurant(site: RestaurantModel) {
    this.sub.push(
      this.addressEditService.editSite(site).subscribe((address) => {
        if (address) {
          this.store.dispatch(restaurantUpdate({ restaurant: address }));
        }
      })
    );
  }

  addNerdSite() {
    const newNerdsite: NerdsiteModel = {
      deleted: false,
      address: '',
      name: '',
      nerdid: Number(this.nerdForm.value.id),
      id: 0,
      x: 0,
      y: 0
    };
    this.addressEditService
      .editSite(newNerdsite)
      .pipe(take(1))
      .subscribe((nerdSite) => {
        if (nerdSite) {
          this.store.dispatch(siteUpdate({ nerdSite }));
        }
      });
  }

  newImage() {
    this.editInPlace = true;
  }

  addRestaurant() {
    const newRestaurant: RestaurantModel = {
      deleted: false,
      address: '',
      name: '',
      phone: '',
      link: '',
      id: 0,
      x: 0,
      y: 0
    };
    this.addressEditService
      .editSite(newRestaurant)
      .pipe(take(1))
      .subscribe((restaurant) => {
        if (restaurant) {
          this.store.dispatch(restaurantUpdate({ restaurant }));
        }
      });
  }

  deleteImage() {
    this.croppedImage = '--';
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
  }
}
