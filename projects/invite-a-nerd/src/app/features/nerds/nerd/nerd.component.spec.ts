import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserTestingModule } from '@angular/platform-browser/testing';
import { CoreModule } from '../../../core/core.module';
import { SettingsModule } from '../../settings/settings.module';

import { NerdComponent } from './nerd.component';

describe('NerdComponent', () => {
  let component: NerdComponent;
  let fixture: ComponentFixture<NerdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserTestingModule, CoreModule, SettingsModule],
      declarations: [NerdComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NerdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
