import { Component, OnInit } from '@angular/core';
import {
  AppState,
  routeAnimations,
  selectAuth
} from '../../../core/core.module';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import { Observable } from 'rxjs';

@Component({
  selector: 'ian-nerd',
  templateUrl: './nerd.component.html',
  styleUrls: ['./nerd.component.scss'],
  animations: [routeAnimations]
})
export class NerdComponent implements OnInit {
  isAuthenticated$: Observable<boolean>;

  nerdSections = [
    { link: 'list', label: 'nerd.listall', auth: true },
    { link: 'new', label: 'nerd.new', auth: true }
  ];
  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.isAuthenticated$ = this.store.pipe(
      select(selectAuth),
      map((auth) => auth.isAuthenticated)
    );
  }
}
