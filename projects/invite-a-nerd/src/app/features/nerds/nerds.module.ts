import { CommonModule } from '@angular/common';
import { NerdComponent } from './nerd/nerd.component';
import { NerdsRoutingModule } from './nerds-routing.module';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { SharedNerdModule } from '../../shared-nerd';
import { NerdsListComponent } from './nerds-list/nerds-list.component';
import { NerdEditComponent } from './nerd-edit/nerd-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { NerdsEffects } from './nerds.effects';
import { ImageCropperModule } from 'ngx-image-cropper';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {
  ModuleTranslateLoader,
  IModuleTranslationOptions
} from '@larscom/ngx-translate-module-loader';

export function moduleHttpLoaderFactory(http: HttpClient) {
  const baseTranslateUrl = `${environment.i18nPrefix}/assets/i18n`;

  const options: IModuleTranslationOptions = {
    lowercaseNamespace: true,
    modules: [
      // final url: ./assets/i18n/en.json
      { baseTranslateUrl },
      // final url: ./assets/i18n/feature1/en.json
      { moduleName: 'nerd', baseTranslateUrl }
    ]
  };

  return new ModuleTranslateLoader(http, options);
}

@NgModule({
  declarations: [NerdComponent, NerdsListComponent, NerdEditComponent],
  imports: [
    SharedModule,
    SharedNerdModule,
    ImageCropperModule,
    CommonModule,
    EffectsModule.forFeature([NerdsEffects]),
    NerdsRoutingModule,
    ReactiveFormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: moduleHttpLoaderFactory,
        deps: [HttpClient]
      },
      isolate: true
    })
  ]
})
export class NerdsModule {}
