import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { State, NerdModel } from '../../../shared-nerd';
import { Store, select } from '@ngrx/store';
import {
  selectAllNerds,
  selectNerdsLoading
} from '../../../shared-nerd/nerd/nerd.selector';
import { nerdLoadAll } from '../../../shared-nerd/nerd/nerd.actions';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import {
  trigger,
  state,
  transition,
  style,
  animate
} from '@angular/animations';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ian-nerds-list',
  templateUrl: './nerds-list.component.html',
  styleUrls: ['./nerds-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state(
        'collapsed',
        style({ height: '0px', minHeight: '0', display: 'none' })
      ),
      state('expanded', style({ height: '100px' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      )
    ])
  ]
})
export class NerdsListComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['name', 'email', 'phone', 'action'];
  nerds$: Observable<NerdModel[]>;
  loading$: Observable<boolean>;
  expandedElement: NerdModel;

  private sub: Subscription[] = [];
  constructor(
    private store: Store<State>,
    private router: Router,
    private breakpointObserver: BreakpointObserver,
    private sanitizer: DomSanitizer
  ) {
    this.nerds$ = this.store.pipe(select(selectAllNerds)).pipe(
      map((nerd) => {
        const rows = [];
        nerd.forEach((element) => {
          rows.push(element, { detailRow: true, element });
        });
        return rows;
      })
    );
    this.loading$ = this.store.pipe(select(selectNerdsLoading));
  }

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map((result) => result.matches));

  ngOnInit() {
    this.store.dispatch(nerdLoadAll());
    this.sub.push(
      this.isHandset$.subscribe((handset) => {
        if (handset) {
          this.displayedColumns = ['name', 'active', 'action'];
        } else {
          this.displayedColumns = [
            'name',
            'email',
            'phone',
            'active',
            'action'
          ];
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.sub.forEach((s) => s.unsubscribe());
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
  }

  editNerd(nerd: NerdModel) {
    this.router.navigate(['nerd/list/', nerd.id]);
  }

  expand(nerd: NerdModel) {
    if (this.expandedElement === nerd) {
      this.expandedElement = null;
    } else {
      this.expandedElement = nerd;
    }
  }

  isExpansionDetailRow = (i: number, row: Object) =>
    row.hasOwnProperty('detailRow');

  sms(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl('sms:' + url);
  }
}
