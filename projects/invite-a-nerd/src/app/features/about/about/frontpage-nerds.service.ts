import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

export interface FrontpageNerds {
  name: string;
  image: string;
  homepageurl: string;
}

@Injectable({
  providedIn: 'root'
})
export class FrontpageNerdsService {
  constructor(private http: HttpClient) {}

  getFrontpageNerds(): Observable<FrontpageNerds[]> {
    return this.http.get<FrontpageNerds[]>(
      environment.backend + '/nerd/frontpage'
    );
  }
}
