import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { ROUTE_ANIMATIONS_ELEMENTS } from '../../../core/core.module';
import { Observable } from 'rxjs';
import {
  FrontpageNerdsService,
  FrontpageNerds
} from './frontpage-nerds.service';

@Component({
  selector: 'ian-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [FrontpageNerdsService]
})
export class AboutComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  nerds$: Observable<FrontpageNerds[]>;

  constructor(private nerdService: FrontpageNerdsService) {
    this.nerds$ = this.nerdService.getFrontpageNerds();
  }

  ngOnInit() {}
}
