import { TestBed } from '@angular/core/testing';

import { FrontpageNerdsService } from './frontpage-nerds.service';

describe('FrontpageNerdsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FrontpageNerdsService = TestBed.inject(
      FrontpageNerdsService
    );
    expect(service).toBeTruthy();
  });
});
