import { Directive, Input } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { NullCheck } from '../../util/null-check';

@Directive({
  selector: '[ianConnectForm]'
})
export class ConnectFormDirective {
  @Input('ianConnectForm')
  set data(val: any) {
    if (NullCheck.isDefinedOrNonNull(val)) {
      this.formGroupDirective.form.patchValue(val);
      this.formGroupDirective.form.markAsPristine();
    }
  }
  constructor(private formGroupDirective: FormGroupDirective) {}
}
