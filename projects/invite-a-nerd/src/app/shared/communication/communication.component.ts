import { Component, OnInit, Input } from '@angular/core';
import { NerdModel } from '../../shared-nerd';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ian-communication',
  templateUrl: './communication.component.html',
  styleUrls: ['./communication.component.css']
})
export class CommunicationComponent implements OnInit {
  @Input()
  nerd: NerdModel;
  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit() {}

  smsLink(phone: string) {
    return this.sanitizer.bypassSecurityTrustUrl('sms:' + phone);
  }
}
