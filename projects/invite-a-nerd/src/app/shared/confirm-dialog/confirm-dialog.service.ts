import { Injectable } from '@angular/core';
import { ConfirmDialogComponent } from './confirm-dialog.component';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDialogService {
  constructor(private dialog: MatDialog) {}

  openConfirmDialog(msg: string): Observable<boolean> {
    return this.dialog
      .open(ConfirmDialogComponent, {
        disableClose: true,
        data: {
          message: msg
        }
      })
      .afterClosed();
  }
}
