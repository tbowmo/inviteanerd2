import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

export interface AdresseLookup {
  type: string;
  tekst: string;
  forslagstekst: string;
  data: {
    id: string;
    href: string;
    vejnavn: string;
    husnr: string;
    supplerendebynavn: string;
    postnr: string;
  };
}

const DAWA_URL = 'https://dawa.aws.dk/adgangsadresser/autocomplete';
const PARAMS = new HttpParams({});

@Injectable()
export class DawaLookupService {
  constructor(private http: HttpClient) {}

  search(term: string): Observable<AdresseLookup[]> {
    if (term === '') {
      return of([]);
    }

    return this.http.get<AdresseLookup[]>(DAWA_URL, {
      params: PARAMS.set('q', term)
    });
  }
}
