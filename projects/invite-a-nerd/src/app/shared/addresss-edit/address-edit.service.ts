import { Injectable } from '@angular/core';
import { AddressEditComponent } from './address-edit.component';
import { Observable } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NerdsiteModel, RestaurantModel } from '../../shared-nerd';

@Injectable({
  providedIn: 'root'
})
export class AddressEditService {
  constructor(public dialog: MatDialog) {}

  editSite(nerdsite: NerdsiteModel): Observable<NerdsiteModel | undefined>;
  editSite(nerdsite: RestaurantModel): Observable<RestaurantModel | undefined>;
  editSite<T>(nerdSite: T): Observable<T | undefined> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = nerdSite;
    return this.dialog.open(AddressEditComponent, dialogConfig).afterClosed();
  }
}
