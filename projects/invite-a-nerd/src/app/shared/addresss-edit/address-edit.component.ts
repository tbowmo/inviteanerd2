import { Observable } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  switchMap,
  take,
  tap
} from 'rxjs/operators';

import { Component, Inject, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';

import {
  AdresseLookup,
  DawaLookupService
} from './dawa-lookup/dawa-lookup.service';
import { NerdsiteModel, RestaurantModel } from '../../shared-nerd';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NullCheck } from '../../../util/null-check';
import { MAT_AUTOCOMPLETE_SCROLL_STRATEGY_FACTORY_PROVIDER } from '@angular/material/autocomplete';

@Component({
  templateUrl: './address-edit.component.html',
  styleUrls: ['./address-edit.component.scss'],
  providers: [
    DawaLookupService,
    MAT_AUTOCOMPLETE_SCROLL_STRATEGY_FACTORY_PROVIDER
  ]
})
export class AddressEditComponent implements OnInit {
  nerdSiteForm: FormGroup;
  filteredAddresses$: Observable<AdresseLookup[]>;
  title = 'Nerdsite';
  constructor(
    private dawa: DawaLookupService,
    private _formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddressEditComponent>,
    @Inject(MAT_DIALOG_DATA) public address: NerdsiteModel | RestaurantModel
  ) {}

  ngOnInit() {
    this.nerdSiteForm = this._formBuilder.group({
      address: ['', [Validators.required, this.addressValidator]],
      name: ['', Validators.required],
      deleted: [false],
      phone: ['', Validators.pattern(/\d{8}/)],
      link: ['']
    });

    if (NullCheck.isDefinedNonNulAndNotEmpty(this.address.address)) {
      this.title = 'Redigér nørdested';
      this.dawa
        .search(this.address.address)
        .pipe(take(1))
        .subscribe((address) => {
          if (address.length > 0) {
            this.nerdSiteForm.get('address').setValue(address[0]);
          }
        });
    }

    if (this.isRestaurant(this.address)) {
      if (NullCheck.isDefinedNonNulAndNotEmpty(this.address.address)) {
        this.title = 'Redigér pizzaria';
      } else {
        this.title = 'Ny pizzaria';
      }
      this.nerdSiteForm.get('phone').enable();
      this.nerdSiteForm.get('link').enable();
    } else {
      this.nerdSiteForm.get('phone').disable();
      this.nerdSiteForm.get('link').disable();
    }

    this.filteredAddresses$ = this.nerdSiteForm
      .get('address')
      .valueChanges.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((addressInput: string) => this.dawa.search(addressInput)),
        tap((address) => {
          if (address.length === 1) {
            this.nerdSiteForm.get('address').setValue(address[0]);
          }
        })
      );
  }

  formatAddress(address: AdresseLookup | string): string | undefined {
    console.log({ address });
    if (typeof address === 'string') {
      return address;
    }
    return address ? address.tekst : undefined;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.nerdSiteForm.invalid) {
      return;
    }
    let modifiedAddress: NerdsiteModel | RestaurantModel = {
      ...this.address,
      address: (this.nerdSiteForm.value.address as AdresseLookup).tekst,
      name: this.nerdSiteForm.value.name,
      deleted: this.nerdSiteForm.value.deleted
    };

    if (this.isRestaurant(this.address)) {
      modifiedAddress = {
        ...modifiedAddress,
        phone: this.nerdSiteForm.value.phone,
        link: this.nerdSiteForm.value.link
      };
    }

    this.dialogRef.close(modifiedAddress);
  }

  addressValidator(control: AbstractControl): { [key: string]: any } | null {
    const forbidden =
      control.value === undefined || control.value.adgangsadresse === undefined;
    return forbidden
      ? { 'incompleteaddress ': { value: control.value } }
      : null;
  }

  isRestaurant(i: NerdsiteModel | RestaurantModel): i is RestaurantModel {
    return 'link' in i;
  }
}
