import { TestBed } from '@angular/core/testing';

import { AddressEditService } from './address-edit.service';

describe('AddressEditService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddressEditService = TestBed.inject(AddressEditService);
    expect(service).toBeTruthy();
  });
});
