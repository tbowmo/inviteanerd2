import { Component, DebugElement } from '@angular/core';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '../shared.module';

@Component({
  selector: 'ian-host-for-test',
  template: ` <ian-spinner [isLoading]="isloading"> </ian-spinner> `
})
class HostComponent {
  isloading = true;
}

describe('SpinnerComponent', () => {
  let fixture: ComponentFixture<HostComponent>;
  let component: HostComponent;
  let spinnerDebug: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HostComponent],
      imports: [SharedModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spinnerDebug = fixture.debugElement.childNodes[0] as DebugElement;
  });

  it('should have expected isLoading', () => {
    expect(component).toBeTruthy();
  });
});
