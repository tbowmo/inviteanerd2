import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { NullCheck } from '../../../util/null-check';
import { SpinnerComponent } from './spinner.component';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  private dialogRef: MatDialogRef<SpinnerComponent>;
  constructor(private dialog: MatDialog) {}

  open() {
    this.dialogRef = this.dialog.open(SpinnerComponent);
  }

  close() {
    if (NullCheck.isDefinedOrNonNull(this.dialogRef)) {
      this.dialogRef.close();
      this.dialogRef = undefined;
    }
  }

  state(state: boolean) {
    if (state) {
      this.open();
    } else {
      this.close();
    }
  }
}
