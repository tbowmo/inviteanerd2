import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatChipsModule } from '@angular/material/chips';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDividerModule } from '@angular/material/divider';
import { MatSliderModule } from '@angular/material/slider';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatTableModule } from '@angular/material/table';
import { MatStepperModule } from '@angular/material/stepper';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDialogModule } from '@angular/material/dialog';

import {
  FontAwesomeModule,
  FaIconLibrary
} from '@fortawesome/angular-fontawesome';
import {
  faPlus,
  faEdit,
  faTrash,
  faTimes,
  faCaretUp,
  faCaretDown,
  faExclamationTriangle,
  faFilter,
  faTasks,
  faCheck,
  faSquare,
  faLanguage,
  faPaintBrush,
  faLightbulb,
  faWindowMaximize,
  faStream,
  faBook,
  faPhone,
  faSms,
  faMapMarker,
  faPizzaSlice,
  faUser,
  faSync,
  faEnvelope,
  faEllipsisH,
  faUserPlus,
  faCreditCard,
  faDollarSign,
  faArrowRight,
  faEllipsisV
} from '@fortawesome/free-solid-svg-icons';
import { faMediumM, faGithub } from '@fortawesome/free-brands-svg-icons';

import { RtlSupportDirective } from './rtl-support/rtl-support.directive';
import { CountTableComponent } from './count-table/count-table.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { AddressEditComponent } from './addresss-edit/address-edit.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { MarkdownModule } from 'ngx-markdown';
import { CommunicationComponent } from './communication/communication.component';
import { ConnectFormDirective } from './connect-form.directive';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    TranslateModule,

    MatButtonModule,
    MatSelectModule,
    MatTabsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatChipsModule,
    MatCardModule,
    MatCheckboxModule,
    MatListModule,
    MatMenuModule,
    MatIconModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatTableModule,
    MatBottomSheetModule,
    ReactiveFormsModule,

    FontAwesomeModule,
    MarkdownModule.forRoot()
  ],
  declarations: [
    RtlSupportDirective,

    CountTableComponent,
    SpinnerComponent,
    AddressEditComponent,
    ConfirmDialogComponent,
    CommunicationComponent,
    ConnectFormDirective
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    TranslateModule,

    MatButtonModule,
    MatMenuModule,
    MatTabsModule,
    MatChipsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatCardModule,
    MatListModule,
    MatSelectModule,
    MatIconModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatSliderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatStepperModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatBottomSheetModule,

    FontAwesomeModule,

    MarkdownModule,

    RtlSupportDirective,

    CountTableComponent,
    SpinnerComponent,
    ConfirmDialogComponent,
    AddressEditComponent,
    CommunicationComponent,
    ConnectFormDirective
  ]
})
export class SharedModule {
  constructor(faIconLibrary: FaIconLibrary) {
    faIconLibrary.addIcons(
      faGithub,
      faMediumM,
      faPlus,
      faEdit,
      faTrash,
      faTimes,
      faCaretUp,
      faCaretDown,
      faExclamationTriangle,
      faFilter,
      faTasks,
      faCheck,
      faSquare,
      faLanguage,
      faPaintBrush,
      faLightbulb,
      faWindowMaximize,
      faStream,
      faBook,
      faPhone,
      faSms,
      faMapMarker,
      faPizzaSlice,
      faUser,
      faSync,
      faEnvelope,
      faEllipsisH,
      faEllipsisV,
      faUserPlus,
      faCreditCard,
      faDollarSign,
      faArrowRight
    );
  }
}
