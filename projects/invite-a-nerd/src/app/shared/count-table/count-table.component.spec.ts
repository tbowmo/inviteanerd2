import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Component, DebugElement } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SharedModule } from '../shared.module';

@Component({
  selector: 'ian-host-for-test',
  template: `
    <ian-count-table [counts]="counts" title="test"> </ian-count-table>
  `
})
class HostComponent {
  counts: MatTableDataSource<[{ label: '1'; count: 2 }]>;
}

describe('CountTableComponent', () => {
  let component: HostComponent;
  let fixture: ComponentFixture<HostComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HostComponent],
      imports: [SharedModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement.childNodes[0] as DebugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
