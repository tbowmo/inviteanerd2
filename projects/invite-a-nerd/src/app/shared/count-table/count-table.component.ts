import { Component, OnInit, Input } from '@angular/core';

export interface CountEntity {
  label: string;
  count: number;
}

@Component({
  selector: 'ian-count-table',
  templateUrl: './count-table.component.html',
  styleUrls: ['./count-table.component.scss']
})
export class CountTableComponent implements OnInit {
  definedColumns = ['label', 'count'];
  @Input()
  counts: CountEntity[] = [];
  @Input()
  title: string;
  constructor() {}

  ngOnInit() {}
}
