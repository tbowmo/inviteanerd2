<?php
date_default_timezone_set("Europe/Copenhagen");
require_once __DIR__ . '/../api/vendor/autoload.php';
// Instantiate the app
$settings = require __DIR__ . '/../api/src/settings.php';
$app = new \Slim\App($settings);
$container = $app->getContainer();
// Register dependencies
require __DIR__ . '/../api/src/dependencies.php';
// Register middleware
function cleanAddress($table, $doLink = false) {
    $data = ORM::for_table($table)->find_many();
    foreach ($data as $i) {
        $street = '';
        $houseno = '';
        $zip = '';
        $city = '';
        
        //$i->street = utf8_decode($i->street);
        //$i->city = utf8_decode($i->city);
        $dataVask = dawaDatavask($i->street, $i->houseno, $i->postalcode, $i->city);
        if ($dataVask->kategori === 'A' || $dataVask->kategori === 'B') {
            $adresse = $dataVask->resultater[0]->adresse;
            $i->street = $adresse->vejnavn;
            $i->houseno = $adresse->husnr;
            $i->postalcode = $adresse->postnr;
            $i->city = $adresse->postnrnavn;
        }
        $i->address = $i->street . ' ' . $i->houseno . ', ' . $i->postalcode . ' ' . $i->city; 
//        $i->name = utf8_decode($i->name);
        $record = dawaRequest(urlencode($i->address));
        print("++++" . $i->address . "\n");
        print_r($record);
        if ($record !== null && array_key_exists(0, $record)) {
            $i->x = floatval($record[0]->x);
            $i->y = floatval($record[0]->y);
        }

        if ($doLink) {
            $i->link = str_replace(array('%3A', '%2F'), array(':', '/'),$i->link);
            $i->phone = str_replace(' ', '', $i->phone);
        }
        $i->save();
    }
}
function dawaDatavask($street, $house, $zip, $city) {
    $url = "https://dawa.aws.dk/datavask/adresser?betegnelse=$street%20$house,$zip%20$city";
    $data = json_decode(file_get_contents($url));
    return $data;
}

function dawaRequest($address) {
    $url = "https://dawa.aws.dk/adgangsadresser?q=$address&struktur=mini";
    $data = json_decode(file_get_contents($url));
    return $data;
}

print("Nerdsite\n");
cleanAddress('nerdsite');
print("Restaurant\n");
cleanAddress('restaurant', true);

$data = ORM::for_table('nerd')->find_many();
foreach($data as $i) {
    $i->phone = str_replace(' ','', $i->phone);
    $i->save();
}

$data = ORM::for_table('invitation')->find_many();
foreach($data as $i) {
    print($i->id);
    print('\n');
//    $i->information = utf8_decode($i->information);
    $i->information = str_replace(array("\\r\\n", "\\n"), array("\r\n", "\n"), $i->information);
    $t = explode(" ", $i->eventstart);
    $i->eventstart = $t[0] . ' ' . $i->homefrom;
    $i->save();
}
/*
$data = ORM::for_table('answer')->find_many();
foreach($data as $i) {
    $i->eatingno = utf8_decode($i->eatingno);
    $i->save();
}
*/