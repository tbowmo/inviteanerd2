<?php
date_default_timezone_set("Europe/Copenhagen");
require_once __DIR__ . '/../api/vendor/autoload.php';
// Instantiate the app
$settings = require __DIR__ . '/../api/src/settings.php';
$app = new \Slim\App($settings);
$container = $app->getContainer();
// Register dependencies
require __DIR__ . '/../api/src/dependencies.php';
// Register middleware

$data = ORM::for_table('nerd')->find_many();
foreach($data as $i) {
    $i->phone = str_replace(' ','', $i->phone);
    $i->save();
}

$data = ORM::for_table('restaurant')->find_many();
foreach($data as $i) {
    $i->phone = str_replace(' ','', $i->phone);
    $i->save();
}
