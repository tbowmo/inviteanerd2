update `nerd`.`guest`
 set noeating=0 where noeating is null;

ALTER TABLE `nerd`.`guest` 
DROP COLUMN `preferedsodaid`,
CHANGE COLUMN `eatingno` `eatingno` VARCHAR(30) NULL DEFAULT NULL ,
CHANGE COLUMN `haspaid` `haspaid` TINYINT(1) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `noeating` `noeating` TINYINT(1) NOT NULL DEFAULT '0' ,
DROP INDEX `preferedSodaId` ;

ALTER TABLE `nerd` 
DROP FOREIGN KEY `drinking`;
ALTER TABLE `nerd` 
DROP COLUMN `preferedsodaid`,
DROP INDEX `drinking` ;

drop table soda;

update guest set eatingno="" where eatingno="''";
