-- 

CREATE TABLE restaurants AS (SELECT restaurant.id,
    name,
    link,
    phone,
    street,
    houseno,
    postalcode,
    city FROM
    restaurant
        LEFT JOIN
    address ON (addressId = address.id));

CREATE TABLE nerdAddress AS (SELECT nerdSite.id,
    nerdid,
    name,
    accessto,
    street,
    houseno,
    postalcode,
    city FROM
    nerdSite
        LEFT JOIN
    address ON (nerdSite.addressid = address.id));

alter table `nerd`.`restaurants`
  add COLUMN address VARCHAR(255) after id,
  add column x FLOAT after city,
  add column y float after city,
  add column deleted int default 0;


alter table `nerd`.`nerdAddress`
  add COLUMN address VARCHAR(255) after id,
  add column x FLOAT after city,
  add column y float after city,
  add column deleted int default 0;


ALTER TABLE `nerd`.`restaurants` 
ADD PRIMARY KEY (`id`),
  CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `nerd`.`nerdAddress` 
ADD PRIMARY KEY (`id`),
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `nerd`.`nerdSiteRestaurant` 
DROP FOREIGN KEY `nerdSiteRestaurant_ibfk_1`,
DROP FOREIGN KEY `nerdSiteResturentN`,
ADD INDEX `nerdSiteRestaurant_ibfk_1_idx` (`restaurantId` ASC),
DROP INDEX `restaurantId` ;

ALTER TABLE `nerd`.`nerdSiteRestaurant` 
ADD CONSTRAINT `nerdSiteRestaurant_ibfk_1`
  FOREIGN KEY (`restaurantId`)
  REFERENCES `nerd`.`restaurants` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `nerd`.`nerdSiteRestaurant` 
ADD INDEX `nerdSiteResturentN_idx` (`nerdSiteId` ASC),
DROP PRIMARY KEY;
ALTER TABLE `nerd`.`nerdSiteRestaurant` 
ADD CONSTRAINT `nerdSiteResturentN`
  FOREIGN KEY (`nerdSiteId`)
  REFERENCES `nerd`.`nerdAddress` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `nerd`.`nerdAddress` 
ADD INDEX `fk_nerdAddress_1_idx` (`nerdid` ASC);
ALTER TABLE `nerd`.`nerdAddress` 
ADD CONSTRAINT `fk_nerdAddress_1`
  FOREIGN KEY (`nerdid`)
  REFERENCES `nerd`.`nerd` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

alter table `nerd`.`invitation`
  add column contribution float,
  add column mobilepay varchar(20),
  add column deleted int default 0,
  add column mobilepaynerdid int(11) null default null;

ALTER TABLE `nerd`.`invitation` 
DROP FOREIGN KEY `invitation_ibfk_1`,
DROP FOREIGN KEY `addr`;


ALTER TABLE `nerd`.`invitation` 
ADD INDEX `invitation_ibfk_1_idx` (`restaurantId` ASC),
DROP INDEX `restaurantId` ;
ALTER TABLE `nerd`.`invitation` 
ADD CONSTRAINT `invitation_ibfk_1`
  FOREIGN KEY (`restaurantId`)
  REFERENCES `nerd`.`restaurants` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
ADD CONSTRAINT `mobilepay`
  FOREIGN KEY (`mobilepaynerdid`)
  REFERENCES `nerd`.`nerd` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `nerd`.`invitation` 
DROP INDEX `addr` ,
ADD INDEX `addr_idx` (`nerdSiteId` ASC);
ALTER TABLE `nerd`.`invitation` 
ADD CONSTRAINT `addr`
  FOREIGN KEY (`nerdSiteId`)
  REFERENCES `nerd`.`nerdAddress` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `nerd`.`nerd`
  add column image VARCHAR(200),
  add column frontpage tinyint default 0,
  add column homepageUrl varchar(200),
  add column suspended tinyint default 0;
  
  
drop table restaurant;

ALTER TABLE `nerd`.`restaurants` 
RENAME TO  `nerd`.`restaurant` ;

drop table `nerd`.`nerdSite`;
drop table `nerd`.`address`;

alter table `nerd`.`nerdAddress`
RENAME TO `nerd`.`nerdsite`;
drop table `nerd`.`answertopping`;


drop TABLE `nerd`.`nerdSiteRestaurant`;
-- ADD PRIMARY KEY (`nerdSiteId`, `restaurantId`);

update answer set attending=2 where attending=1;
update answer set attending=1 where attending=0;

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tokens` (
  `iduser` int(11) DEFAULT NULL,
  `token` varchar(8192) DEFAULT NULL,
  `datecreated` int NULL DEFAULT NULL,
  `dateexpiration` int NULL DEFAULT NULL,
  KEY `fk_tokens_1_idx` (`idUser`),
  CONSTRAINT `fk_tokens_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `nerd`.`user`
(`username`,
`password`)
VALUES
('nerdclub',
'avrman');

ALTER TABLE `nerd`.`invitation` 
CHANGE COLUMN `date` `eventstart` DATETIME NOT NULL ;


ALTER TABLE `nerd`.`invitation` 
DROP FOREIGN KEY `addr`,
DROP FOREIGN KEY `host`,
DROP FOREIGN KEY `invitation_ibfk_1`;
ALTER TABLE `nerd`.`invitation` 
CHANGE COLUMN `homeFrom` `homefrom` TINYTEXT NOT NULL ,
CHANGE COLUMN `hostId` `hostid` INT(11) NOT NULL ,
CHANGE COLUMN `restaurantId` `restaurantid` INT(11) NOT NULL ,
CHANGE COLUMN `nerdSiteId` `nerdsiteid` INT(11) NOT NULL ;
ALTER TABLE `nerd`.`invitation` 
ADD CONSTRAINT `addr`
  FOREIGN KEY (`nerdsiteid`)
  REFERENCES `nerd`.`nerdsite` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `host`
  FOREIGN KEY (`hostid`)
  REFERENCES `nerd`.`nerd` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `invitation_ibfk_1`
  FOREIGN KEY (`restaurantid`)
  REFERENCES `nerd`.`restaurant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;



ALTER TABLE `nerd`.`nerd` 
DROP FOREIGN KEY `drinking`;
ALTER TABLE `nerd`.`nerd` 
CHANGE COLUMN `preferedSodaId` `preferedsodaid` INT(11) NULL DEFAULT '0' ,
CHANGE COLUMN `mailNotification` `mailnotification` INT(1) NOT NULL DEFAULT '1' ,
CHANGE COLUMN `homepageUrl` `homepageurl` VARCHAR(200) NULL DEFAULT NULL ;
ALTER TABLE `nerd`.`nerd` 
ADD CONSTRAINT `drinking`
  FOREIGN KEY (`preferedsodaid`)
  REFERENCES `nerd`.`soda` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;




ALTER TABLE `nerd`.`answer` 
DROP FOREIGN KEY `from`,
DROP FOREIGN KEY `to`;
ALTER TABLE `nerd`.`answer` 
CHANGE COLUMN `nerdId` `nerdid` INT(11) NOT NULL ,
CHANGE COLUMN `invitationId` `invitationid` INT(11) NOT NULL ,
CHANGE COLUMN `eatingNo` `eatingno` TINYTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `hasPaid` `haspaid` TINYINT(4) NOT NULL,
add column `noeating` tinyint(4) default 0 ;

ALTER TABLE `nerd`.`answer` 
ADD CONSTRAINT `from`
  FOREIGN KEY (`nerdid`)
  REFERENCES `nerd`.`nerd` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `to`
  FOREIGN KEY (`invitationid`)
  REFERENCES `nerd`.`invitation` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;



ALTER TABLE `nerd`.`eating` 
DROP FOREIGN KEY `eatingI`,
DROP FOREIGN KEY `eatingN`,
DROP FOREIGN KEY `eatingT`;
ALTER TABLE `nerd`.`eating` 
CHANGE COLUMN `nerdId` `nerdid` INT(11) NOT NULL ,
CHANGE COLUMN `invitationId` `invitationid` INT(11) NOT NULL ,
CHANGE COLUMN `toppingId` `toppingid` INT(11) NOT NULL ;
ALTER TABLE `nerd`.`eating` 
ADD CONSTRAINT `eatingI`
  FOREIGN KEY (`invitationid`)
  REFERENCES `nerd`.`invitation` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `eatingN`
  FOREIGN KEY (`nerdid`)
  REFERENCES `nerd`.`nerd` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `eatingT`
  FOREIGN KEY (`toppingid`)
  REFERENCES `nerd`.`topping` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
