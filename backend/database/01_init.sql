-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: 172.25.02    Database: nerd
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `answer` (
  `nerdid` int(11) NOT NULL,
  `invitationid` int(11) NOT NULL,
  `eatingno` tinytext,
  `price` float NOT NULL,
  `haspaid` tinyint(4) NOT NULL,
  `attending` tinyint(1) NOT NULL DEFAULT '0',
  `noeating` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nerdid`,`invitationid`),
  KEY `to` (`invitationid`),
  CONSTRAINT `from` FOREIGN KEY (`nerdid`) REFERENCES `nerd` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `to` FOREIGN KEY (`invitationid`) REFERENCES `invitation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eating`
--

DROP TABLE IF EXISTS `eating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eating` (
  `nerdid` int(11) NOT NULL,
  `invitationid` int(11) NOT NULL,
  `toppingid` int(11) NOT NULL,
  PRIMARY KEY (`nerdid`,`invitationid`,`toppingid`),
  KEY `eatingI` (`invitationid`),
  KEY `eatingT` (`toppingid`),
  CONSTRAINT `eatingI` FOREIGN KEY (`invitationid`) REFERENCES `invitation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eatingN` FOREIGN KEY (`nerdid`) REFERENCES `nerd` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `eatingT` FOREIGN KEY (`toppingid`) REFERENCES `topping` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `guest`
--

DROP TABLE IF EXISTS `guest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guest` (
  `nerdid` int(11) NOT NULL,
  `invitationid` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `eatingno` varchar(30) DEFAULT NULL,
  `price` float DEFAULT '0',
  `haspaid` tinyint(1) NOT NULL DEFAULT '0',
  `noeating` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nerdid`,`invitationid`,`id`),
  KEY `id` (`id`),
  KEY `answerNerdId` (`nerdid`),
  KEY `answerInvitationId` (`invitationid`),
  CONSTRAINT `guest_ibfk_1` FOREIGN KEY (`nerdid`) REFERENCES `nerd` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `guest_ibfk_2` FOREIGN KEY (`invitationid`) REFERENCES `invitation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `guestEating`
--

DROP TABLE IF EXISTS `guestEating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guestEating` (
  `guestid` int(11) NOT NULL,
  `toppingid` int(11) NOT NULL,
  PRIMARY KEY (`guestid`,`toppingid`),
  KEY `toppingId` (`toppingid`),
  KEY `fk_guestEating_1_idx` (`guestid`),
  CONSTRAINT `fk_guestEating_1` FOREIGN KEY (`guestid`) REFERENCES `guest` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `guestEating_ibfk_4` FOREIGN KEY (`toppingid`) REFERENCES `topping` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `invitation`
--

DROP TABLE IF EXISTS `invitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invitation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventstart` datetime NOT NULL,
  `information` text NOT NULL,
  `hostid` int(11) NOT NULL,
  `restaurantid` int(11) DEFAULT NULL,
  `nerdsiteid` int(11) NOT NULL,
  `contribution` float DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `mobilepaynerdid` int(11) DEFAULT NULL,
  `mobilepay` varchar(10) DEFAULT '''''',
  PRIMARY KEY (`id`),
  KEY `host` (`hostid`),
  KEY `invitation_ibfk_1_idx` (`restaurantid`),
  KEY `addr_idx` (`nerdsiteid`),
  KEY `mobilepay_idx` (`mobilepaynerdid`),
  CONSTRAINT `addr` FOREIGN KEY (`nerdsiteid`) REFERENCES `nerdsite` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `host` FOREIGN KEY (`hostid`) REFERENCES `nerd` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `invitation_ibfk_1` FOREIGN KEY (`restaurantid`) REFERENCES `restaurant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mobilepay` FOREIGN KEY (`mobilepaynerdid`) REFERENCES `nerd` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nerd`
--

DROP TABLE IF EXISTS `nerd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nerd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `homepageurl` varchar(200) DEFAULT NULL,
  `frontpage` tinyint(1) DEFAULT '0',
  `mailnotification` tinyint(1) NOT NULL DEFAULT '1',
  `suspended` tinyint(1) DEFAULT '0',
  `donoteatpizza` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nerdsite`
--

DROP TABLE IF EXISTS `nerdsite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nerdsite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `nerdid` int(11) NOT NULL,
  `name` tinytext CHARACTER SET utf8 NOT NULL,
  `accessto` tinyint(1) NOT NULL DEFAULT '1',
  `y` float DEFAULT NULL,
  `x` float DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_nerdAddress_1_idx` (`nerdid`),
  CONSTRAINT `fk_nerdAddress_1` FOREIGN KEY (`nerdid`) REFERENCES `nerd` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `restaurant`
--

DROP TABLE IF EXISTS `restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `name` tinytext CHARACTER SET utf8 NOT NULL,
  `link` tinytext CHARACTER SET utf8,
  `phone` tinytext CHARACTER SET utf8,
  `y` float DEFAULT NULL,
  `x` float DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tokens` (
  `iduser` int(11) DEFAULT NULL,
  `token` varchar(8192) DEFAULT NULL,
  `datecreated` int(11) DEFAULT NULL,
  `dateexpiration` int(11) DEFAULT NULL,
  KEY `fk_tokens_1_idx` (`iduser`),
  CONSTRAINT `fk_tokens_1` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `topping`
--

DROP TABLE IF EXISTS `topping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `topping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-05 20:06:39
