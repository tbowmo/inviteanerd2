<?php
// DIC configuration
require_once (__DIR__ . '/settings.db.php');

$container = $app->getContainer();
// monolog
/*$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};
*/
$container['secret'] = "nerdAppSecret";

ORM::configure($db_connection);
ORM::configure('username', $db_username);
ORM::configure('password', $db_password);
ORM::configure('id_column_overrides', array(
    'answer' => array('nerdid', 'invitationid'),
    'eating' => array('nerdid', 'invitationid', 'toppingid')
));