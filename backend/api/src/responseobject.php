<?php

function dawaRequest($address) {
    $a = urlencode($address->address);
    $url = "https://dawa.aws.dk/adgangsadresser?q=$a&struktur=mini";
    $record = json_decode(file_get_contents($url));
    if ($record !== null && array_key_exists(0, $record)) {
        $address->x = floatval($record[0]->x);
        $address->y = floatval($record[0]->y);
    }
    return $address;
}

function createNerdObject($nerd) {
    $n = $nerd->as_array();
    $n['donoteatpizza'] = ($n['donoteatpizza'] == '1')? true: false; 
    $n['suspended'] = ($n['suspended'] == '1')? true: false;
    $n['frontpage'] = ($n['frontpage'] == '1')? true: false;
    $n['id'] = $n['id'] * 1;
    return $n;
  }