<?php
require __DIR__ . '/responseobject.php';
require __DIR__ . '/routes/authenticate.php';
require __DIR__ . '/routes/invite.php';
require __DIR__ . '/routes/nerd.php';
require __DIR__ . '/routes/brew.php';
require __DIR__ . '/routes/user.php';
require __DIR__ . '/routes/answer.php';
require __DIR__ . '/routes/version.php';
require __DIR__ . '/routes/nerdsite.php';
require __DIR__ . '/routes/restaurant.php';
require __DIR__ . '/routes/topping.php';
