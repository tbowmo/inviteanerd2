<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/version', function(Request $request, Response $response, $args) {
    $composerFile = file_get_contents('composer.json');
    $composerJson = json_decode($composerFile, true);
    return $response->withJson($composerJson['version']);
});
?>