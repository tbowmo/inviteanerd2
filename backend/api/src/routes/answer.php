<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->group('/answer', function() use ($app) {
  $app->get('/{id}', function(Request $request, Response $response, $args) {
    $inviteId = $args['id'];
    $answers = [];
    $nerds = ORM::for_table('nerd')->raw_query('select nerd.id as nerdid, coalesce(attending, 0) as attending, coalesce(eatingno, "") as eatingno, coalesce(price, 0) as price, noeating, haspaid from nerd left join (select * from answer where invitationid=' . $inviteId . ') as answer on (nerd.id=nerdid) order by attending desc, name')->find_many();
    foreach ($nerds as $n) {
      $x = createAnswer($n, $inviteId);

      $answers[] = $x;
    }
    return $response->withJson($answers);
  });

  $app->post('/update', function(Request $request, Response $response) {
    $data = $request->getParsedBody();
    if($data['nerdid'] == null ||
      $data['invitationid'] == null) {
        return $response->withStatus(400);
    }
    $s = Array(
      'invitationid' => $data['invitationid'],
      'nerdid' => $data['nerdid']);

    $answer = ORM::for_table('answer')->find_one($s);

    if ($answer == false) {
      $answer = ORM::for_table('answer')->create();
      $answer->nerdid = $data['nerdid'];
      $answer->invitationid = $data['invitationid'];
    }
    if ($data['price'] == null) $data['price'] = 0;
    $answer->attending = $data['attending'];
    $answer->eatingno = $data['eatingno'];
    $answer->price = $data['price'];
    $answer->haspaid = $data['haspaid'] ? '1': '0';
    $answer->noeating = $data['noeating'] ? '1' : '0';
    $answer->save();
    $top = ORM::for_table('eating')
      ->where_equal('nerdid', $data['nerdid'])
      ->where_equal('invitationid', $data['invitationid'])
      ->delete_many();
    if ($data['topping'] !== null) {
      foreach($data['topping'] as $t) {
        $top = ORM::for_table('eating')->create();
        $top->nerdid = $data['nerdid'];
        $top->invitationid = $data['invitationid'];
        $top->toppingid = $t;
        $top->save();
      } 
    }
    

    return $response->withJson(getAnswer($data['nerdid'], $data['invitationid']));
  });

  $app->get('/pizzahistory/{nerd}/{restaurant}', function(Request $request, Response $response, $args) {
    $nerdId = $args['nerd'];
    $restaurant = $args['restaurant'];
    $query = sprintf('SELECT 
          answer.eatingno as eating
      FROM
          answer
              LEFT JOIN
          invitation ON (invitation.id = answer.invitationid)
      where answer.noeating=0 and answer.eatingno is not null and answer.nerdid=%s and invitation.restaurantid=%s group by eatingno;
      ', $nerdId, $restaurant);
    $orders =  ORM::for_table('answer')->raw_query($query)->find_many();
    $orderArray = [];
    foreach ($orders as $order) {
      $orderArray[] = $order->eating;
    }
    return $response->withJson($orderArray);
  });

  $app->post('/addOrUpdateGuest', function(Request $request, Response $response) {
    $data = $request->getParsedBody();
    if ($data['nerdid'] == null ||
    $data['invitationid'] == null ||
    $data['name' == null]) {
      return $response->withStatus(400);
    }
    $guest = ORM::for_table('guest')->find_one($data['id']);
    if ($guest == false) {
      $guest = ORM::for_table('guest')->create();
      $guest->nerdid = $data['nerdid'];
      $guest->invitationid = $data['invitationid'];
    }
    $guest->name = $data['name'];
    if ($data['price'] == null) $data['price'] = 0;
    if ($data['eatingno'] != null)
      $guest->eatingno = $data['eatingno'];
    $guest->price = $data['price'];
    $guest->haspaid = $data['haspaid'] ? '1': '0';
    $guest->noeating = $data['noeating'] ? '1' : '0';
    $guest->save();

    ORM::for_table('guestEating')
      ->where_equal('guestid', $data['id'])
      ->delete_many();

    if ($data['topping'] !== null) {
      foreach($data['topping'] as $t) {
        $top = ORM::for_table('guestEating')->create();
        $top->guestid = $data['id'];
        $top->toppingid = $t;
        $top->save();
      }
    }

    return $response->withJson(getAnswer($data['nerdid'], $data['invitationid']));
  });

  $app->post('/deleteGuest', function(Request $request, Response $response) {
    $data = $request->getParsedBody();
    $guest = ORM::for_table('guest')->find_one($data['id']);
    $guest->delete();
    return $response->withJson(getAnswer($data['nerdid'], $data['invitationid']));
  });
});

function createGuest($guest) {
  $guest = $guest->as_array();
  $guest['nerdid'] = (int) $guest['nerdid'];
  $guest['haspaid'] = (boolean) $guest['haspaid'];
  $guest['invitationid'] = (int) $guest['invitationid'];
  $guest['noeating'] = (boolean) $guest['noeating'];
  $guest['id'] = (int) $guest['id'];
  if ($guest['eatingno'] == null) {
    $guest['eatingno'] = '';
  }
  $t = ORM::for_table('guestEating')
    ->where_equal('guestid', $guest['id'])
    ->find_many();
  $guest['topping'] = Array();
  foreach($t as $topping) {
    $guest['topping'][] = (int) $topping->toppingid;
  }
  return $guest;
}

function createAnswer($n, $inviteId) {
  $x = $n->as_array();
  $nerd = ORM::for_table('nerd')->find_one($x['nerdid']);
  $x['nerd'] = createNerdObject($nerd);
  $x['name'] = $x['nerd']['name'];
  $x['attending'] = (int) $x['attending'];
  $x['haspaid'] = (boolean) $x['haspaid'];
  $x['nerdid'] = (int) $x['nerdid'];
  $x['noeating'] = (boolean) $x['noeating'];
  $x['invitationid'] = (int) $inviteId;
  $x['guest'] = [];
  $t = ORM::for_table('guest')
    ->where('invitationid', $inviteId)
    ->where('nerdid', $x['nerdid'])
    ->find_many();
  foreach ($t as $guest) {
    $x['guest'][] = createGuest($guest);
  }
  $t = ORM::for_table('eating')
    ->where_equal('nerdid', $x['nerdid'])
    ->where_equal('invitationid', $x['invitationid'])
    ->find_many();
  $x['topping'] = Array();
  foreach($t as $topping) {
    $x['topping'][] = (int) $topping->toppingid;
  }
  return $x;
}

function getAnswer($nerdId, $invitationId) {
  $n = ORM::for_table('nerd')->raw_query('select nerd.id as nerdid, coalesce(attending, 0) as attending, coalesce(eatingno, "") as eatingno, coalesce(price, 0) as price, noeating, haspaid from nerd left join (select * from answer where invitationid=' . $invitationId . ') as answer on (nerd.id=nerdid) where nerd.id=' . $nerdId)->find_one($nerdId);
  return createAnswer($n, $invitationId);
}