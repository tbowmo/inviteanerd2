<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$app->group('/nerd', function() use ($app) {
  $app->get('/frontpage', function(Request $request, Response $response) use ($app) {
    $nerds = ORM::for_table('nerd')->
      where('suspended', 0)->
      where('frontpage', 1)->
      select('name')->
      select('image')->
      select('homepageurl')->
      order_by_asc('name')->find_many();
    $results = Array();
    foreach($nerds as $n) {
      $result[] = $n->as_array();
    }
    return $response->withJson($result);
  });

  $app->get('/list', function (Request $request, Response $response, $args) {
    $decoded = $request->getAttribute("token");
    $nerds = ORM::for_table('nerd')->order_by_asc('name')->find_many();
    $res = Array();
    foreach($nerds as $n) {
      $res[] = CreateNerdObject($n);
    }
    return $response->withJson($res);
  });

  $app->get('/{id}', function (Request $request, Response $response, $args) {
    $nerdId = $args['id'];
    $nerd = ORM::for_table('nerd')->find_one($nerdId);
    return $response->withJson(createNerdObject($nerd));
  });

  $app->post('', function (Request $request, Response $response) {
    $data = $request->getParsedBody();

    if ($data['name'] == null ||
        $data['phone'] == null ||
        $data['email'] == null) {
          return $response->withStatus(400);
    }

    $nerd = ORM::for_table('nerd')->find_one($data['id']);

    if ($nerd == false) {
      $nerd = ORM::for_table('nerd')->create();
      $nerd->name = $data['name'];
      $nerd->save();
    }

    if ($data['imageData'] != null) {
      if ($data['imageData'] == "--") {
        $nerd->image = null;
      } else {
        $img = base64_decode($data['imageData']);
        $fname = "images/". $nerd->id."_". time() .".png";
        $fp = fopen($fname, 'w');
        fwrite($fp, $img);
        fclose($fp);
        $nerd->image = "/api/$fname";
      }
    }
    $nerd->name = $data['name'];
    $nerd->phone = $data['phone'];
    $nerd->email = $data['email'];
    $nerd->homepageurl = $data['homepageurl'];
    $nerd->suspended = $data['suspended'] ? '1' : '0';
    $nerd->frontpage = $data['frontpage'] ? '1' : '0';
    $nerd->donoteatpizza = $data['donoteatpizza'] ? '1' : '0';
    $nerd->save();
    return $response->withStatus(201)->withJson(createNerdObject($nerd));
  });

  $app->delete('/{id}', function (Request $request, Response $response, $args) {
    $nerdId = $args['id'];
    $nerd = ORM::for_table('nerd')->find_one($nerdId);
    if ($nerd) {
      $nerd->suspended = 1;
      $nerd->save();
      return $response->withStatus(204)->withJson($nerd->as_array());
    }
    return  $response->withStatus(400);
  });
});

