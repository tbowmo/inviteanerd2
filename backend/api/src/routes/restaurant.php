<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$app->group('/restaurant', function() use ($app) {
    $app->get('/{nerdSiteId}[/{distance}]', function(Request $request, Response $response, $args) {
        $nerdSiteId = $args['nerdSiteId'] * 1;
        $distance = floatval($args['distance']);
        $nerdSite = ORM::for_table('nerdsite')->find_one($nerdSiteId);
        $lat = $nerdSite->y;
        $long = $nerdSite->x;
        $sql = "
            select * from (
            SELECT a.*, 
            111.111 *
                DEGREES(ACOS(COS(RADIANS(a.y))
                    * COS(RADIANS($lat))
                    * COS(RADIANS(a.x - $long))
                    + SIN(RADIANS(a.y))
                    * SIN(RADIANS($lat)))) AS distance
            FROM restaurant as a) as b where distance < $distance;
            ";
        try {
            $res = ORM::for_table('restaurant')->raw_query($sql)->find_many();
            $x = array();
            foreach ($res as $r) {
                $address = $r->as_array();
                $address['id'] = $address['id'] * 1;
                $address['x'] = $x['x'] * 1;
                $address['y'] = $x['y'] * 1;
                $address['deleted'] = ($address['deleted'] == '1') ? true: false;      
                $x[] = $address;
            }
            return $response->withJson($x);
        } catch (Exception $e) {
            return $response->withJson([]);
        }
    })->setArgument('distance', "25");


    $app->post('', function(Request $request, Response $response) {
        $data = $request->getParsedBody();
        $site = ORM::for_table('restaurant')->find_one($data['id']);
        if ($site === false) {
            $site = ORM::for_table('restaurant')->create();
        }
        if ($data['name'] == null ||
            $data['address'] == null) {
                return $response->withStatus(400);
            }
        $site->name = $data['name'];
        $site->address = $data['address'];
        $site->phone = $data['phone'];
        $site->link = $data['link'];
        $site->deleted = $data['deleted'] ? '1': '0';
        $site = dawaRequest($site);
        $site->save();
        return $response->withStatus(201)->withJson($site->as_array());
    });
});