<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Firebase\JWT\JWT;

$app->post('/authenticate', function (Request $request, Response $response) {
    $data = $request->getParsedBody();
    $login = $data['username'];
    $password = hash('sha512', $data['password']);
    $current_user = ORM::for_table('user')
                    ->where('username', $login)
                    ->where('password', $password)
                    ->find_one();
    if ($current_user === false) {
        echo json_encode("No user found");
    } else {
        $token_from_db = ORM::for_table('tokens')->where('iduser', $current_user->iduser)->where_gt('dateexpiration', time())->find_one();
        if ($token_from_db !== false) {
            echo json_encode([
                "token"      => $token_from_db->token,
                "user_login" => $token_from_db->iduser
            ]);
        }
        // Create a new token if a user is found but not a token corresponding to whom.
        if ($token_from_db === false) {
            $key = $this->get("secret");
            $payload = array(
                "iss"     => "https://inviteanerd.nerdclub.dk",
                "iat"     => time(),
                "exp"     => time() + (3600 * 24 * 15),
                "context" => [
                    "user" => [
                        "user_login" => $current_user->username,
                        "user_id"    => $current_user->iduser
                    ]
                ]
            );
            try {
                $jwt = JWT::encode($payload, $key);
                $token = ORM::for_table('tokens')->create();
                $token->iduser = $current_user->iduser;
                $token->token = $jwt;
                $token->datecreated = $payload['iat'];
                $token->dateexpiration = $payload['exp'];
                $token->save();
                echo json_encode([
                    "token"      => $jwt,
                    "user_login" => $current_user->iduser
                ]);
            } catch (Exception $e) {
                echo json_encode($e);
            }
        }
    }
});
