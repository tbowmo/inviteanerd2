<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->group('/invite', function() use ($app) {
  $app->get('/list', function (Request $request, Response $response) {
    $invitations = ORM::for_table('invitation')
              ->order_by_desc('eventstart')
              ->find_many();
    $res = Array();
    foreach($invitations as $i) {
      $res[] = createInviteObj($i);
    }
    return $response->withJson($res);
  });

  $app->get('/next', function(Request $request, Response $response) {
    $invitations = ORM::for_table('invitation')
              ->where('deleted', '0')
              ->where_raw('eventstart >= curdate()') 
              ->order_by_asc('eventstart')
              ->find_one();
    if ($invitations) {
      return $response->withJson(createInviteObj($invitations));
    } else {
      return $response->withStatus(204);
    }
  });

  $app->get('/{id}', function(Request $request, Response $response, $args) {
    $inviteid = $args['id'];
    $invitation = ORM::for_table('invitation')->find_one($inviteid);
    if (!$invitation) {
      return $response->withStatus(404);
    }
    return $response->withJson(createInviteObj($invitation));
  });

  $app->post('', function(Request $request, Response $response) {
    $data = $request->getParsedBody();
    $invitation = ORM::for_table('invitation')->find_one($data['id']);
    if ($invitation === false) {
      $invitation = ORM::for_table('invitation')->create();
    }
    if($data['eventstart'] == null ||
      $data['information'] == null ||
      $data['nerdsiteid'] == null ||
      $data['hostid'] == null) {
        return $response->withStatus(400);
      }
    $date = new DateTime($data['eventstart']);
    $invitation->eventstart = $date->format('Y-m-d H:i:s');
    $invitation->information = $data['information'];
    $invitation->nerdsiteid = $data['nerdsiteid'];
    $invitation->hostid = $data['hostid'];
    $invitation->restaurantid = $data['restaurantid'];
    $invitation->contribution = $data['contribution'];
    $invitation->mobilepay = $data['mobilepay'];
    $invitation->mobilepaynerdid = $data['mobilepaynerdid'];
    $invitation->save();
    return $response->withJson(createInviteObj($invitation));
  });

  $app->delete('/{id}', function(Request $request, Response $response, $args){
    $inviteid = $args['id'];
    $invitation = ORM::for_table('invitation')->find_one($inviteid);
    $invitation->deleted = true;
    $invitation->save();
    return $response->withJson(createInviteObj($invitation));
  });
});

function createInviteObj($invitation) {
  $c = $invitation->as_array();
  $c['id'] = $c['id'] * 1;
  $d = new DateTime($c['eventstart'], new DateTimeZone('UTC'));
  $c['eventstart'] = $d->format(DateTime::ATOM);
  $c['contribution'] = $c['contribution'] * 1;
  $nerd = ORM::for_table('nerd')->find_one($invitation->hostid);
  $c['nerd'] = $nerd->as_array();
  if ($invitation->mobilepaynerdid != null) {
    $mobilepay = ORM::for_table('nerd')->find_one($invitation->mobilepaynerdid);
    $c['mobilepaynerd'] = $mobilepay->as_array();
  }
  $nerdsite = ORM::for_table('nerdsite')->find_one($invitation->nerdsiteid);
  $c['nerdsite'] = $nerdsite->as_array();
  $c['nerdsite']['x'] = $c['nerdsite']['x'] * 1;
  $c['nerdsite']['y'] = $c['nerdsite']['y'] * 1;
  if ($invitation->restaurantid != null) {
    $restaurant = ORM::for_table('restaurant')->find_one($invitation->restaurantid);
    $c['restaurant'] = $restaurant->as_array();
    $c['restaurant']['x'] = $c['restaurant']['x'] * 1;
    $c['restaurant']['y'] = $c['restaurant']['y'] * 1;
  }
  $c['deleted'] = ($c['deleted'] == '1') ? true: false;
  return $c;
}
