<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->post('/user/create', function (Request $request, Response $response) {
  $data = $request->getParsedBody();
  $newUser = ORM::for_table('user')->create();
  if ($data['email'] == null || $data['password'] == null) {
    return $response->withStatus(400, 'no password / email specified');
  }
  else {
    $newUser->email = $data['email'];
    $newUser->password = $data['password'];
    $newUser->save();
    return $response->withJson($newUser->as_array());
  }
});

$app->post('/user/passwordchange', function(Request $request, Response $response) {
  $data = $request->getParsedBody();
  $user = ORM::for_table('user')->find_one($decoded->context->user->user_id);
  if ($user == null) {
    return $response->withStatus(400, "User unknown");
  } else {
    if ($user->getPassword() == $data['oldpass']) {
      $user->setPassword($data['newpass']);
      $user->save();
      return $response->withStatus(204);
    } else {
      return $response->withStatus(400, "old password does not match");
    }
  }
});
