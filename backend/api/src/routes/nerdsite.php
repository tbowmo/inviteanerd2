<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$app->group('/nerdsite', function() use ($app) {
    $app->get('/{nerdId}[/{deleted}]', function (Request $request, Response $response, $args) {
        $nerdId = $args['nerdId'];
        $deleted = $args['deleted'];
        $addressList = ORM::for_table('nerdsite')->where_lte('deleted', $deleted)->where('nerdid', $nerdId)->find_many();
        $result = Array();
        foreach($addressList as $address) {
            $address = $address->as_array();
            $address['id'] = $address['id'] * 1;
            $address['nerdid'] = $address['nerdid'] * 1;
            $address['x'] = $x['x'] * 1;
            $address['y'] = $x['y'] * 1;
            $address['deleted'] = ($address['deleted'] == '1') ? true: false;
            $result[] = $address;
        }
        return $response->withJson($result);
    })->setArgument('deleted', "0");

    $app->post('', function (Request $request, Response $response) {
        $data = $request->getParsedBody();
        $site = ORM::for_table('nerdsite')->find_one($data['id']);
        if ($site === false) {
            $site = ORM::for_table('nerdsite')->create();
        }
        if ($data['nerdid'] == null ||
            $data['name'] == null ||
            $data['address'] == null) {
                return $response->withStatus(400);
            }
        $site->nerdid = $data['nerdid'];
        $site->name = $data['name'];
        $site->deleted = ($data['deleted'] ? '1': '0');
        $site->address = $data['address'];
        $site = dawaRequest($site);
        $site->save();
        return $response->withStatus(201)->withJson($site->as_array());
    });
});