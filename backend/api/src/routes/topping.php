<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/topping', function (Request $request, Response $response) {
  $decoded = $request->getAttribute("token");
  $brews = ORM::for_table('topping')->order_by_asc('name')->find_many();
  $res = Array();
  foreach($brews as $brew) {
    $res[] = $brew->as_array();
  }
  return $response->withJson($res);
});

$app->post('/topping', function (Request $request, Response $response) {
  $data = $request->getParsedBody();
  $brew = ORM::for_table('soda')->find_one($data['id']);
  if ($brew === false) {
    $brew = ORM::for_table('soda')->create();
  }
  $brew->name = $data['name'];
  $brew->save();
  return $response->withJson($brew->as_array());
});
