<?php
// Application middleware
// e.g: $app->add(new \Slim\Csrf\Guard);
// A middleware for enabling CORS
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->add(new \Slim\Middleware\JwtAuthentication([
    "secret" => $container->get("secret"),
    "callback" => function($request, $response, $arguments) use ($container) {
      $container['jwt'] = $arguments["decoded"];
    },
    "error" => function ($request, $response, $arguments) {
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
        return $response
        ->withHeader("Content-Type", "application/json")->withStatus(401)
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    },
    "path" => "/",
    "passthrough" => ["/authenticate", "/version", "/nerd/frontpage" ],
]));
